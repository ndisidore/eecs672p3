// ShaderIF.h: Basic interface to read, compile, and link GLSL Shader programs

#ifndef SHADERIF_H
#define SHADERIF_H

#include <GL/gl.h>

#include <string>

class ShaderIF
{
public:
	// EITHER create an instance with the following constructor and
	// use the getShaderPgmID() method to get the shader ID like:
	//       ShaderIF sIF(vShaderFileName, fShaderFileName);
	//       GLuint pgmId = sIF.getShaderPgmID();
	ShaderIF(const std::string& vShader, const std::string& fShader);
	virtual ~ShaderIF();
	GLuint getShaderPgmID() const { return shaderPgm; }

	// OR simply use the following class method like:
	//       GLuint pgmId = ShaderIF::initShader(vShaderFileName, fShaderFileName);
	static GLuint initShader(const std::string& vShader, const std::string& fShader);

	// In either event, the caller is responsible for deleting the returned
	// program when done. That is, it must eventually do:
	//        glDeleteProgram(sID)
	// where "sID" is the id returned from getShaderPgmID() or initShader.
private:
	struct Shader
	{
		std::string fName;
		GLenum sType;
		std::string source;
		GLuint pgmID;
	};
	void initShaders();

	static bool readShaderSource(Shader& shader);

	GLuint shaderPgm;
	int numShaderComponents;
	Shader* shaders;
};

#endif
