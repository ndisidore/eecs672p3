/* 
Name: Nathan Disidore
ID: 2112424
Class: EECS 672
Project 3

// SouthPark.h - a concrete subclass of ModelView that draws a Kenny from South Park
*/

#ifndef SOUTHPARK_H
#define SOUTHPARK_H

#include <GL/gl.h>
#include <GL/freeglut.h>
#include <iostream>

#include "ModelView.h"

class SouthPark : public ModelView
{
public:
	SouthPark(const cryph::AffPoint origin);
	virtual ~SouthPark();

	void getWCBoundingBox(float* xyzLimits) const; // {xmin, xmax, ymin, ymax, zmin, zmax}
	void render();
	void handleKey(unsigned char key);

private:
	static GLint const numManMeshes = 21;
	static GLint const numSSMeshes = 21;

	//bool isKennyDead;

	//List of meshes, which hold all our data
	manualMesh* manMeshes[numManMeshes];		//list of manual meshes
	simpleShapeMesh* ssMeshes[numSSMeshes];		//list of simple shape meshes

	int nextManMeshIndex, 	// next manual mesh slot to be allocated
		nextSSMeshIndex,	// next simpleShape mesh slot to be allocated
		// special slots we want to remember:
		headIndex,			//0 - hoodie/head
		leftEyeIndex,		//4 - left eye
		rightEyeIndex,  	//5 - right eye
		leftEyeBallIndex,	//6 - left eyeball
		rightEyeBallIndex,	//7 - right eyeball
		underShirtIndex,	//index of brown undershirt
		drawstringIndex,	//index of ring around hoodie/hoodie drawstring
		torsoIndex,			//1 - index of torso/legs
		rightArmIndex,		//3 - index of right arm
		leftArmIndex,		//2 - index of left arm
		rArmLineIndex,
		lArmLineIndex,
		rightHandIndex,		//index of right hand
		leftHandIndex,		//index of left hand
		rightThumbIndex,	//index of right thumb
		leftThumbIndex,		//index of left thumb
		zipperIndex,		//index of zipper
		feetIndex,
		rFaceSkinIndex,
		lFaceSkinIndex,
		lowerDStringIndex;

	static GLuint shaderProgram; 	// all instances use same shader program
	
	//Attribute location maps
	static GLint
		aLoc_mcPosition,			//modeling coordinate position
		aLoc_mcNormal,				//modeling coordinate normal
		aLoc_texCoords;				//texture coordinates

	//Uniform location maps
	static GLint
		uLoc_modelViewMatrix,		//Model View Matrix
		uLoc_projectionMatrix,		//Projection Matrix
		uLoc_normalMatrix,			//Normal Matrix
		uLoc_ecLightPosition,		//position of light source
		uLoc_actualNumLights,		//actual number of lights in scene
		uLoc_lightStrength,			//strength of each color in light source
		uLoc_globalAmbient,			//strength of each color in ambient light
		uLoc_spotDir,				//direction of lighting spotlight
		uLoc_spotParams,			//
		uLoc_kakdks,				//RGBA of ambient, diffuse, specular reflection
		uLoc_m,						//Shininess of primitive
		uLoc_haveTextureMap,		//does material use a texture?
		uLoc_textureMap;			//t,s mapping of texture

	//setup code
	void doInitialize();
	void setupVars();
	void defineLightingEnvironment();

	//Body part definitions
	void defineAllKenny();
	//void defineEyes();
	void defineHead();
	void defineTorso();
	void defineArms();
	//void defineFaceSkin();
	void defineLines();
	void defineHandsFingers();
	void defineFeet();

	//manual Mesh functions
	void handleManualMeshVAO(manualMesh *mesh, int& index);
	void cleanManualMesh(manualMesh *mesh);
	void setManualMeshColor(manualMesh *mesh, vec4 ka, vec4 kd, vec4 ks);
	void invertXCoord(manualMesh* oldMesh, manualMesh* newMesh);
	void drawManualMesh(manualMesh* object);

	//Simple Shape Mesh Functions
	void handleSSMeshVAO(simpleShapeMesh *mesh, int& index);
	void setSSMeshColor(simpleShapeMesh *mesh, vec4 ka, vec4 kd, vec4 ks);
	void defineTexture(simpleShapeMesh* object);
	void setTextureImage(simpleShapeMesh* object, const char* imgFileName);
	void drawSimpleShape(simpleShapeMesh* object);

	//2D Geometries
	void pwlEllipse(const vec3 center, GLfloat xR, GLfloat yR, int numVerts, manualMesh* mesh);

	//3D Specific Geometry definitions
	//void genSphere(const vec3 center, float r, int lats, int longs, Mesh* mesh);
	void genSphere(const vec3 center, float r, int p, manualMesh* mesh);
	void genEllipse(const vec3 center, float xR, float yR, float zR, int p, manualMesh* mesh);
	void genCylinder(const vec3 center, float radius, float length, float slices, manualMesh* mesh);
	void genDiskCylinder(const vec3 center, const vec3 offset, int numDisks, float radius, float slices, manualMesh* mesh);

};

#endif
