/* 
Name: Nathan Disidore
ID: 2112424
Class: EECS 672
Project 3
*/
// ModelView.h - an Abstract Base Class for a combined Model and View for OpenGL
//
// This version illustrates a very minimal set of base class functions for a set
// of concrete subclasses that can support a simple window-viewport map:
// 1. The pure virtual method "getWCBoundingBox" is designed for a Controller to ask the
//    subclass what its extent is so that the Controller can keep track of an overall
//    bounding box that encloses the entire scene.

#ifndef MODELVIEW_H
#define MODELVIEW_H

#include <GL/gl.h>
#include <fstream>

#include "../PointVectorTools/AffPoint.h"
#include "../PointVectorTools/AffVector.h"
#include "../PointVectorTools/MatrixUtil.h"
#include "../SimpleShape/SimpleShape.h"
#include "../ImageReader/ImageReader.h"

typedef GLfloat vec2[2]; // or use Angel's definitions
typedef GLfloat vec3[3]; // or use Angel's definitions
typedef GLfloat vec4[4]; // or use Angel's definitions

const int numAttributes = 2;
//this is for EACH mesh/object generated
typedef struct simpleShapeMesh
        {
                simpleShapeMesh() : meshShape(NULL), texImage(NULL), texID(0)
                {
                        glGenVertexArrays(1, &vao);
                }
                std::string name;               //Identifier of mesh
            SimpleShape* meshShape;             //pointer to simpleshape object
                ImageReader* texImage;          //pointer to the image reader object with texture info
                GLuint vao;                     //vertex array object in question, for bindArray
                GLuint buffer[numAttributes+1]; //holds buffer data for bindBuffer/bufferData
                                                //[+1 for texture coords]
                GLuint texID;                   //holds texture buffer data
                float kakdks[12];               //ambient, diffuse and specular reflection rgbs
        } simpleShapeMesh;

typedef struct manualMesh
        {
                manualMesh() : vertexArray(NULL), normalArray(NULL), elementCount(0)
                {
                        glGenVertexArrays(1, &vao);
                }
                std::string name;               //Identifier of mesh
                GLenum drawMode;                //drawmode of mesh
                vec3 *vertexArray;              //pointer to list of vertexes
                vec3 *normalArray;              //pointer to list of vertex normals
                GLsizei elementCount;           //number of vertexes for the given mesh
                GLuint vao;                     //vertex array object in question, for bindArray
                GLuint buffer[numAttributes];   //holds buffer data for bindBuffer/bufferData
                float kakdks[12];               //ambient, diffuse and specular reflection rgbs
        } manualMesh;

class ModelView
{
public:
	ModelView();
	virtual ~ModelView();

	virtual void getWCBoundingBox(float* xyzLimits) const = 0; // { xmin, xmax, ymin, ymax, zmin, zmax }
	virtual void render() = 0;
	virtual void handleKey(unsigned char key) = 0;
	void scaleGlobalScale(float scaleFraction);
	void addToGlobalPan(float deltaX, float deltaY, float deltaZ);
	void addToGlobalRotateDegrees(float screenRotX, float screenRotY, float screenRotZ);

protected:
	cryph::AffPoint modelOrigin;	//model origin point

	/*****************************************************************
	* Declaring all the variables necessary for creating the matrices
	*****************************************************************/

	//Scale Perspective translation variables
	//At point
	vec3 atPoint;

	//Vectors used in creating model view matrix
	cryph::AffVector upVec;

	//Vectors used for model view matrix
	cryph::AffVector uVec;
	cryph::AffVector vVec;
	cryph::AffVector wVec;

	//distance from eye to center used for pre/post translation
	//and initial build of modelview
	float distanceEyeCenter;

	//View Port boundaries
	float xMin, xMax, yMin, yMax, zMin, zMax;

	//Unit vectors
	cryph::AffVector xUnit;		//(1,0,0);
	cryph::AffVector yUnit;		//(0,1,0);
	cryph::AffVector zUnit;		//(0,0,1);

	//Points used for creating the matrices
	cryph::AffPoint eyeCenter, attentionCenter;

	//necessary matrices
	vec4 modelViewMatrix[4];	//ModelView
	vec3 normalMatrix[3];		//Normal Matrix
	vec4 projectionMatrix[4];	//Projection Matrix
	vec4 panMatrix[4];			//Model Panning Matrix
	vec4 preTransMatrix[4];		//Pre Translation Matrix
	vec4 postTransMatrix[4];	//Post Translation Matrix
	vec4 scaleMatrix[4];		//Scaling Matrix
	vec4 dynamicRotation[4];	//Model rotation
	vec4 wcToECMatrix[4];		//World Coord -> Eye Coord Mapping

	//wanted projection type
	bool projectionType;
		//0 = orthogonal
		//1 = perspective

	//matrix computations
	void setupMatrices();
	void updateMatrices();
	void createWCtoECmap();
	void updateNormalMatrix();
	void setIdentityMatrix(float* matrix, int dem);
	void copyMatrix(float* from, float* to, int dem);
	void anglesToAxes(const vec3 angles, cryph::AffVector& left, cryph::AffVector& up, cryph::AffVector& forward);
	void perspectiveProjection(float xmin, float xmax, float ymin, float ymax, float zmax, float zmin, float zpp);
	void orthogonalProjection(float xmin, float xmax, float ymin, float ymax, float zmax, float zmin);

private:
	//static void linearMap(float fromMin, float fromMax, float toMin, float toMax, float* scaleTrans);

};

#endif

