/* 
Name: Nathan Disidore
ID: 2112424
Class: EECS 672
Project 3
*/
// Controller.h - a basic Controller (in Model-View-Controller sense)

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>


#include "ModelView.h"

class Controller
{
public:
	Controller(const std::string& name);
	virtual ~Controller();

	void addModel(ModelView* m);

	static bool checkForErrors(std::ostream& os, const std::string& context);
	// following returns a bounding box that contains all models managed by this Controller
	static void getOverallWCBoundingBox(float* xyzLimits);
	static void reportVersions(std::ostream& os);


private:
	static int const maxClickVerts = 10;
	bool mouseMotionIsTranslate;			//was the mouse motion a translations?

	Controller(const Controller& c) {} 		//do not allow copies, including pass-by-value
	std::vector<ModelView*> models;

	float clickCoords[maxClickVerts][2];  	//2D array for where the clicks occur

	//track screenbase x and y
	int screenBaseX,	//X Pos
		screenBaseY;	//Y Pos

	int vpWidth, vpHeight;
	float overallWCBoundingBox[6];
	bool overallWCBoundingBoxInitialized;
	bool recordClicksSet;					//check to see if click recording is currently active
	int recordClickVerts;					//counter for added record Click Verts

	void doDisplay();
	void doReshape();
	void doMouseMotion(int x, int y);
	void doMouseFunc(int button, int state, int x, int y);
	void establishInitialCallbacksForRC();
	void initializeOpenGLRC();
	void updateWCBoundingBox(ModelView* m);
	void identifyPos(int x, int y);;
	void shutdownPgm();

	static Controller* curController;

	static int createWindow(const std::string& windowTitle);
	static void displayCB();
	static void keyboardCB(unsigned char key, int x, int y);
	static void reshapeCB(int width, int height);
	static void mouseFuncCB(int button, int state, int x, int y);
	static void mouseMotionCB(int x, int y);
};

#endif
