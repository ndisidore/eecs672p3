/* 
Name: Nathan Disidore
ID: 2112424
Class: EECS 672
Project 3
*/
// ModelView.c++ - an Abstract Base Class for a combined Model and View for OpenGL

#include "ModelView.h"
#include "Controller.h"
#include "../PointVectorTools/AffPoint.h"
#include "../PointVectorTools/AffVector.h"
#include "../PointVectorTools/MatrixUtil.h"

ModelView::ModelView() :
	projectionType(false)
{
	//call matrix setup function
	setupMatrices();
}

ModelView::~ModelView()
{
}

/*
 * Creating the model view matrix based on the position of the WC and the eye coordinates
 */
void ModelView::createWCtoECmap()
{
	//Set x components
	wcToECMatrix[0][0] = uVec.dot(xUnit);
	wcToECMatrix[0][1] = uVec.dot(yUnit);
	wcToECMatrix[0][2] = uVec.dot(zUnit);
	wcToECMatrix[0][3] = 0;

	//Set y components
	wcToECMatrix[1][0] = vVec.dot(xUnit);
	wcToECMatrix[1][1] = vVec.dot(yUnit);
	wcToECMatrix[1][2] = vVec.dot(zUnit);
	wcToECMatrix[1][3] = 0;

	//Set z components
	wcToECMatrix[2][0] = -wVec.dot(xUnit);
	wcToECMatrix[2][1] = -wVec.dot(yUnit);
	wcToECMatrix[2][2] = -wVec.dot(zUnit);
	wcToECMatrix[2][3] = -distanceEyeCenter;

	//Set bottom row
	wcToECMatrix[3][0] = 0;
	wcToECMatrix[3][1] = 0;
	wcToECMatrix[3][2] = 0;
	wcToECMatrix[3][3] = 1;

	std::cout << "The WC to EC Matrix" << std::endl;
	showMatrix(std::cout, wcToECMatrix);
}

/*
 * Update the normalMatrix based on the modelView Matrix
 */
void ModelView::updateNormalMatrix()
{
	/* the normal matrix is the inverse transpose of the upper
	   3x3 portion of the modelView matrix*/
	vec3 upperThree[3], upperInverse[3], upperInverseTranspose[3];
	upper3x3(modelViewMatrix, upperThree);		//TMPMVIEW
	inverse(upperThree, upperInverse);
	transpose(upperInverse, upperInverseTranspose);
	copyMatrix(&upperInverseTranspose[0][0], &normalMatrix[0][0], 3);

	std::cout << "The Normal Matrix" << std::endl;
	showMatrix(std::cout, normalMatrix);
}

void ModelView::perspectiveProjection(float xmin, float xmax, float ymin, float ymax, float zmax, float zmin, float zpp)
{
	/*********************************************************
	 *
	 *  PERSPECTIVE PROJECTION MATRIX COMPUTATION
	 *
	 *  | Ax*Zpp	0 		Bx 		0  |
	 *  | 0 		Ay*Zpp 	By 		0  |
	 *  | 0  		0  		Az 		Bz |
	 *  | 0  		0  		0  		1  |
	 *
	 *  Ax: (2.0)/(xmax-xmin)	Bx: (xmax+xmin)/(xmax-xmin)
	 *  Ay: (2.0)/(ymax-ymin)	By: (ymax+ymin)/(ymax-ymin)
	 *  Az: (-2.0*zmin*zmax)/(zmax-zmin)
	 *  	Bz: (zmin+zmax)/(zmin-zmax)
	 *
	 *  Computes the orthogonal projection of the scene
	 *
	 *********************************************************/
	//This is in row-major notation!

	//row one
	projectionMatrix[0][0] = (-2.0*zpp)/(xmax-xmin);
	projectionMatrix[0][1] = 0;
	projectionMatrix[0][2] = (xmax+xmin)/(xmax-xmin);
	projectionMatrix[0][3] = 0;

	//row two
	projectionMatrix[1][0] = 0;
	projectionMatrix[1][1] = (-2.0*zpp)/(ymax-ymin);
	projectionMatrix[1][2] = (ymax+ymin)/(ymax-ymin);
	projectionMatrix[1][3] = 0;

	//row three
	projectionMatrix[2][0] = 0;
	projectionMatrix[2][1] = 0;
	projectionMatrix[2][2] = (zmax+zmin)/(zmax-zmin);
	projectionMatrix[2][3] = (-2.0*zmin*zmax)/(zmax-zmin);

	//row four
	projectionMatrix[3][0] = 0;
	projectionMatrix[3][1] = 0;
	projectionMatrix[3][2] = -1.0;
	projectionMatrix[3][3] = 0;

	std::cout << "Perspective Projection Matrix:" << std::endl;
	showMatrix(std::cout, projectionMatrix);
}

void ModelView::orthogonalProjection(float xmin, float xmax, float ymin, float ymax, float zmax, float zmin)
{
	/*********************************************************
	 *
	 *  ORTHOGONAL PROJECTION MATRIX COMPUTATION
	 *
	 *  | Ax 0  0  Bx |
	 *  | 0  Ay 0  By |
	 *  | 0  0  Az Bz |
	 *  | 0  0  0  1  |
	 *
	 *  Ax: (2.0)/(xmax-xmin)	Bx: -(xmax+xmin)/(xmax-xmin)
	 *  Ay: (2.0)/(ymax-ymin)	By: -(ymax+ymin)/(ymax-ymin)
	 *  Az: (2.0)/(zmin-zmax)	Bz: -(zmin+zmax)/(zmin-zmax)
	 *
	 *  Computes the orthogonal projection of the scene
	 *
	 *********************************************************/
	//This is in row major notation!

    //row one
	projectionMatrix[0][0] = (2.0)/(xmax-xmin);
	projectionMatrix[0][1] = 0;
	projectionMatrix[0][2] = 0;
	projectionMatrix[0][3] = -(xmax+xmin)/(xmax-xmin);

	//row two
	projectionMatrix[1][0] = 0;
	projectionMatrix[1][1] = (2.0)/(ymax-ymin);
	projectionMatrix[1][2] = 0;
	projectionMatrix[1][3] = -(ymax+ymin)/(ymax-ymin);

	//row three
	projectionMatrix[2][0] = 0;
	projectionMatrix[2][1] = 0;
	projectionMatrix[2][2] = (2.0)/(zmin-zmax);
	projectionMatrix[2][3] = -(zmin+zmax)/(zmin-zmax);

	//row four
	projectionMatrix[3][0] = 0;
	projectionMatrix[3][1] = 0;
	projectionMatrix[3][2] = 0;
	projectionMatrix[3][3] = 1.0;

	std::cout << "Orthogonal Projection Matrix:" << std::endl;
	showMatrix(std::cout, projectionMatrix);
}

void ModelView::setupMatrices()
{
	//panning matrix
	//		Px,Py,Pz automatically set to 0
	setIdentityMatrix(&panMatrix[0][0], 4);
        std::cout << "Setup Panning Matrix" << std::endl;
        showMatrix(std::cout, panMatrix);

	//pre translation matrix
	setIdentityMatrix(&preTransMatrix[0][0], 4);
	preTransMatrix[2][3] = 1.0;		//set d to be arbitrary 1.0
        std::cout << "Setup Pre-Translation Matrix" << std::endl;
        showMatrix(std::cout, preTransMatrix);

	//post translation matrix
	setIdentityMatrix(&postTransMatrix[0][0], 4);
	postTransMatrix[2][3] = -1.0;	//set d to be arbitrary 1.0
        std::cout << "Setup Post-Translation Matrix" << std::endl;
        showMatrix(std::cout, postTransMatrix);

	//scaling matrix
	//		Sx, Sy, Sz automatically set to 1
	setIdentityMatrix(&scaleMatrix[0][0], 4);
        std::cout << "Setup Scaling Matrix" << std::endl;
        showMatrix(std::cout, scaleMatrix);

	//dynamic rotation matrix
	setIdentityMatrix(&dynamicRotation[0][0], 4);
        std::cout << "Setup dynamicRotation Matrix" << std::endl;
        showMatrix(std::cout, dynamicRotation);
}

void ModelView::scaleGlobalScale(float scaleFraction)
{
	/*********************************************************
	 *
	 *  SCALE MATRIX COMPUTATION
	 *       S
	 *  | s 0 0 0 |
	 *  | 0 s 0 0 |
	 *  | 0 0 s 0 |
	 *  | 0 0 0 1 |
	 *
	 *  s: scaleFraction
	 *
	 *  As simple as multiplying the current
	 *  scaleMatrix by the scale Fraction in correct spots
	 *
	 *********************************************************/
	scaleMatrix[0][0] = scaleMatrix[0][0]*scaleFraction;		//New X Scale
	scaleMatrix[1][1] = scaleMatrix[1][1]*scaleFraction;		//New Y Scale
	scaleMatrix[2][2] = scaleMatrix[2][2]*scaleFraction;		//New Z Scale
}

void ModelView::addToGlobalPan(float deltaX, float deltaY, float deltaZ)
{
	/*********************************************************
	 *
	 *  PAN MATRIX COMPUTATION
	 *       P
	 *  | 1 0 0 Px |
	 *  | 0 1 0 Py |
	 *  | 0 0 1 Pz |
	 *  | 0 0 0 1  |
	 *
	 *  Px: deltaX
	 *  Py: deltaY
	 *  Pz: deltaZ
	 *
	 *  As simple as adding the new offset to the current
	 *  panMatrix in correct spots
	 *
	 *********************************************************/
	//As simple as adding the new offsets to the current
	//panMatrix in correct spots
	panMatrix[0][3] = panMatrix[0][3]+deltaX;		//New X Pan
	panMatrix[1][3] = panMatrix[1][3]+deltaY;		//New Y Pan
	panMatrix[2][3] = panMatrix[2][3]+deltaZ;		//New Z Pan
}

void ModelView::addToGlobalRotateDegrees(float rx, float ry, float rz)
{
	///////////////////////////////////////////////////////////////////////////////
	// convert Euler angles(x,y,z) total global rotation
	// Each column of the rotation matrix represents left, up and forward axis.
	// The order of rotation is Roll->Yaw->Pitch (Rx*Ry*Rz)
	// Rx: rotation about X-axis, pitch
	// Ry: rotation about Y-axis, yaw(heading)
	// Rz: rotation about Z-axis, roll
	//    Rx           Ry          Rz
	// |1  0   0| | Cy  0 Sy| |Cz -Sz 0|   | CyCz        -CySz         Sy  |
	// |0 Cx -Sx|*|  0  1  0|*|Sz  Cz 0| = | SxSyCz+CxSz -SxSySz+CxCz -SxCy|
	// |0 Sx  Cx| |-Sy  0 Cy| | 0   0 1|   |-CxSyCz+SxSz  CxSySz+SxCz  CxCy|
	///////////////////////////////////////////////////////////////////////////////
	/*From: http://www.songho.ca/opengl/gl_anglestoaxes.html*/

    const float DEG2RAD = 3.141593f / 180;
    float sx, sy, sz, cx, cy, cz, theta;

    // rotation angle about X-axis (pitch)
    theta = rx * DEG2RAD;
    sx = sin(theta);
    cx = cos(theta);

    // rotation angle about Y-axis (yaw)
    theta = ry * DEG2RAD;
    sy = sin(theta);
    cy = cos(theta);

    // rotation angle about Z-axis (roll)
    theta = rz * DEG2RAD;
    sz = sin(theta);
    cz = cos(theta);

    float newRotation[4][4];
    // column 0
    newRotation[0][0] = cy*cz;
    newRotation[1][0] = sx*sy*cz + cx*sz;
    newRotation[2][0] = -cx*sy*cz + sx*sz;
    newRotation[3][0] = 0;

    // column 1
    newRotation[0][1] = -cy*sz;
    newRotation[1][1] = -sx*sy*sz + cx*cz;
    newRotation[2][1] = cx*sy*sz + sx*cz;
    newRotation[3][1] = 0;

    // column 2
    newRotation[0][2] = sy;
    newRotation[1][2] = -sx*cy;
    newRotation[2][2] = cx*cy;
    newRotation[3][2] = 0;

    // column 3
    newRotation[0][3] = 0;
    newRotation[1][3] = 0;
    newRotation[2][3] = 0;
    newRotation[3][3] = 1;

    float tmpDynamic[4][4];
    //now actually do the multiplication
    multiply(newRotation, dynamicRotation, tmpDynamic);
    //and copy the holder back to dynamicRotation
    copyMatrix(&tmpDynamic[0][0], &dynamicRotation[0][0], 4);
}

void ModelView::updateMatrices()
{
	std::cout << "-----MATRIX UPDATING PROCESS------" << std::endl;

	//need to create the map from WC to EC
	createWCtoECmap();

	float phase1[4][4], phase2[4][4], phase3[4][4], dynamic[4][4];
	//dynamic = P * T[post] * dynamicRotation * S * T[pre]
	//Step 1: S * T[pre]
	//first update preTransMatrix with current distanceEyeCenter
	preTransMatrix[2][3] = distanceEyeCenter;
	multiply(scaleMatrix, preTransMatrix, phase1);
    std::cout << "Step 1: S * T[pre] Matrix" << std::endl;
    showMatrix(std::cout, phase1);

	//Step 2: dynamicRotation * phase1
	multiply(dynamicRotation, phase1, phase2);
    std::cout << "Step 2: dynamicRotation * phase1 Matrix" << std::endl;
    showMatrix(std::cout, phase2);

	//Step 3: T[post] * phase2
	//first update postTransMatrix with current distanceEyeCenter
	postTransMatrix[2][3] = -distanceEyeCenter;
	multiply(postTransMatrix, phase2, phase3);
    std::cout << "Step 3: T[post] * phase2 Matrix" << std::endl;
    showMatrix(std::cout, phase3);

	//Step 4: dynamic = P * phase3
	multiply(panMatrix, phase3, dynamic);
    std::cout << "Step 4: dynamic = P * phase3 Matrix" << std::endl;
    showMatrix(std::cout, dynamic);

	//modelViewMatrix = dynamic * wcToECMatrix
	multiply(dynamic, wcToECMatrix, modelViewMatrix);
    std::cout << "The ModelView Matrix" << std::endl;
    showMatrix(std::cout, modelViewMatrix);

	//normal is the same
	updateNormalMatrix();
}

void ModelView::setIdentityMatrix(float* matrix, int dem)
{
	for(int i=0; i<dem; i++)
	{
		for(int j=0; j<dem; j++)
		{
			if(i==j)
				matrix[i*dem+j] = 1.0;
			else
				matrix[i*dem+j] = 0.0;
		}
	}
}
void ModelView::copyMatrix(float* from, float* to, int dem)
{
	for(int i=0; i<dem; i++)
	{
		for(int j=0; j<dem; j++)
		{
			to[i*dem+j] = from[i*dem+j];
		}
	}
}

