/* 
Name: Nathan Disidore
ID: 2112424
Class: EECS 672
Project 1

// scene.c++ - drives the opengl/glut process
*/

#include <iostream>

#include <GL/freeglut.h>

#include "SouthPark.h"

#include "Controller.h"

int main(int argc, char* argv[])
{
	// One-time initialization of the glut
	glutInit(&argc, argv);

	Controller c(argv[0]);
	cryph::AffPoint origin = cryph::AffPoint(0.0, 0.0, 0.0);
	ModelView* m = NULL;
	float xyz[6], rowSize = -1.0;

	m = new SouthPark(origin);
	if (m != NULL)
	{
		c.addModel(m);
		m->getWCBoundingBox(xyz);
	}

	// Hand off control to the glut event handling loop:
	glutMainLoop();

	return 0;
}
