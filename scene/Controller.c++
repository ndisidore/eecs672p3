/* 
Name: Nathan Disidore
ID: 2112424
Class: EECS 672
Project 3

Controller.c++: a basic Controller (in Model-View-Controller sense)
*/

#include <GL/freeglut.h>

#include "Controller.h"
#include "ModelView.h"

Controller* Controller::curController = NULL;

Controller::Controller(const std::string& name)
	: 	overallWCBoundingBoxInitialized(false),
	  	recordClicksSet(false), recordClickVerts(0),
	  	mouseMotionIsTranslate(true)
{
	curController = this;

	// First create the window and its Rendering Context (RC)
	int windowID = createWindow(name);

	// Then initialize the newly created OpenGL RC
	establishInitialCallbacksForRC(); // the callbacks for this RC
	initializeOpenGLRC(); // other general OpenGL initialization for the RC
	
	// generic default
	overallWCBoundingBox[0] = overallWCBoundingBox[2] = overallWCBoundingBox[4] = -1.0;
	overallWCBoundingBox[1] = overallWCBoundingBox[3] = overallWCBoundingBox[5] =  1.0;

}

Controller::~Controller()
{
	if (this == curController)
		curController = NULL;
}

void Controller::addModel(ModelView* m)
{
	models.push_back(m);
	updateWCBoundingBox(m);
}

bool Controller::checkForErrors(std::ostream& os, const std::string& context)
	// CLASS METHOD
{
	bool hadError = false;
	GLenum e = glGetError();
	while (e != GL_NO_ERROR)
	{
		os << "CheckForErrors (context: " <<  context
		   << "): " << (char*)gluErrorString(e) << std::endl;
		e = glGetError();
		hadError = true;
	}
	return hadError;
}

int Controller::createWindow(const std::string& windowTitle) // CLASS METHOD
{
	// The following "glutInitContext*" calls guarantee that we use only
	// modern OpenGL functionality.
	glutInitContextVersion(3,3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);

	// Now create the window and its Rendering Context.
	int windowID = glutCreateWindow(windowTitle.c_str());
	return windowID;
}

void Controller::displayCB() // CLASS METHOD
{
	if (curController != NULL)
		curController->doDisplay();
}

void Controller::doDisplay()
{
	// clear the frame buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (std::vector<ModelView*>::iterator it=models.begin() ; it<models.end() ; it++)
		(*it)->render();

	// flush the graphics buffers
	glutSwapBuffers();

	checkForErrors(std::cout, "Controller::doDisplay");
}

void Controller::doReshape()
{
	glViewport(0, 0, vpWidth, vpHeight);
}

void Controller::establishInitialCallbacksForRC()
{
	glutDisplayFunc(displayCB); 
	glutReshapeFunc(reshapeCB);
	glutKeyboardFunc(keyboardCB);
	glutMouseFunc(mouseFuncCB);
	glutMotionFunc(mouseMotionCB);
}

void Controller::getOverallWCBoundingBox(float* xyzLimits)
{
	for (int i=0 ; i<6 ; i++)
		xyzLimits[i] = curController->overallWCBoundingBox[i];
}

void Controller::initializeOpenGLRC() 
{
	glEnable(GL_DEPTH_TEST);   // Enables depth-buffer for hidden surface removal
	glDepthFunc(GL_LEQUAL);    // The type of depth testing to do
	glClearColor(0.2, 0.2, 0.4, 1.0); // set the background color
	glClearDepth(1.0);        // Set clear depth value to farthest
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
}

void Controller::keyboardCB(unsigned char key, int x, int y)
{
	static const unsigned char ESC = 27; // ASCII code for the ESC key
	if (key == ESC)
	{
		Controller::curController->shutdownPgm();
	}
	else if (key == 'p')
	{
		Controller::curController->models[0]->handleKey(key);
		glutPostRedisplay();
	}
}

void Controller::identifyPos(int x, int y)
{
	//Original of form:
	//double fy = static_cast<double>(vpHeight-y)/static_cast<double>(vpHeight);

	//this gets the mapping on the (-1,-1):(1,1) grid that the logical device
	//space has
	double fx = 2.0*((static_cast<double>(x)/static_cast<double>(vpWidth))-(.5));
	double fy = -2.0*((static_cast<double>(y)/static_cast<double>(vpHeight))-(.5)); //need to invert y to account for axis flip

	if(recordClicksSet && recordClickVerts < maxClickVerts)
	{
		std::cout << std::endl << recordClickVerts <<  "--- X: " << fx << "  &&  " << "Y: " << fy << std::endl << std::endl;
		clickCoords[recordClickVerts][0] = fx;
		clickCoords[recordClickVerts][1] = fy;
		recordClickVerts++;

		for(int i=0; i<recordClickVerts; i++)
		{
			std::cout << clickCoords[i][0] << " " << clickCoords[i][1] << std::endl;
		}
	}

}

void Controller::mouseFuncCB(int button, int state, int x, int y)
{
	Controller::curController->doMouseFunc(button, state, x, y);
}

void Controller::mouseMotionCB(int x, int y)
{
	Controller::curController->doMouseMotion(x, y); 
}

void Controller::doMouseFunc(int button, int state, int x, int y)
{
	/*FROM: http://people.eecs.ku.edu/~miller/Courses/OpenGL/3D/ControllerCodeForHandlingEvents.c++*/
	// I usually use:
	//     scaleFraction  = 1.1;
	//
	// An alternative to the way scaling is handled below:
	//     ModelView::addToGlobalScale( scaleIncrement); // to scale up
	//     ModelView::addToGlobalScale(-scaleIncrement); // to scale down
	//     (I usually use: scaleIncrement = 0.1; Don't let globalScale get <= 0)
	float scaleFraction = 1.1;
	static const int SCROLL_WHEEL_UP = 3;
	static const int SCROLL_WHEEL_DOWN = 4;
	if (button == SCROLL_WHEEL_UP)
	{
		// each wheel click generates a state==DOWN and state==UP event; use only one
		if (state == GLUT_DOWN)
		{
			for (std::vector<ModelView*>::iterator m=models.begin() ; m<models.end() ; m++)
				(*m)->scaleGlobalScale(scaleFraction);
			glutPostRedisplay();
		}
	}
	else if (button == SCROLL_WHEEL_DOWN)
	{
		// each wheel click generates a state==DOWN and state==UP event; use only one
		if (state == GLUT_DOWN)
		{
			for (std::vector<ModelView*>::iterator m=models.begin() ; m<models.end() ; m++)
				(*m)->scaleGlobalScale(1.0/scaleFraction);
			glutPostRedisplay();
		}
	}
	else
	{
		mouseMotionIsTranslate = ((glutGetModifiers() & GLUT_ACTIVE_SHIFT) != 0);
		if (state == GLUT_DOWN)
		{
			Controller::curController->screenBaseX = x; Controller::curController->screenBaseY = y;
		}
	}
}

void Controller::doMouseMotion(int x, int y)
{
	/*FROM: http://people.eecs.ku.edu/~miller/Courses/OpenGL/3D/ControllerCodeForHandlingEvents.c++*/
	// if (xmin,xmax,ymin,ymax) are the values used in the last orthogonal or perspective
	// function, and (width, height) are the values used in the last glViewport call (also
	// query-able using glGetIntegerv with type=GL_VIEWPORT), then:
	// wcPerPixelX = (xmax - xmin) / width;
	// wcPerPixelY = (ymax - ymin) / height;
	float wcPerPixelX = (1 + 1) / Controller::curController->vpWidth;
	float wcPerPixelY = (1 + 1) / Controller::curController->vpHeight;
	int dx = (x - Controller::curController->screenBaseX), dy = (y - Controller::curController->screenBaseY);
	Controller::curController->screenBaseX = x; Controller::curController->screenBaseY = y;
	if (mouseMotionIsTranslate)
		for (std::vector<ModelView*>::iterator m=models.begin() ; m<models.end() ; m++)
			(*m)->addToGlobalPan(dx*wcPerPixelX, -dy*wcPerPixelY, 0.0);
	else
	{
		static const float pixelsToDegrees = 360.0 / 500.0;
		float screenRotY = pixelsToDegrees * dx;
		float screenRotX = pixelsToDegrees * dy;
		for (std::vector<ModelView*>::iterator m=models.begin() ; m<models.end() ; m++)
			(*m)->addToGlobalRotateDegrees(screenRotX, screenRotY, 0.0);
	}
	glutPostRedisplay();
}

void Controller::shutdownPgm()
{
	exit(0);
}

void Controller::reportVersions(std::ostream& os)
{
	const char* glVer = reinterpret_cast<const char*>(glGetString(GL_VERSION));
	const char* glslVer = reinterpret_cast<const char*>
			(glGetString(GL_SHADING_LANGUAGE_VERSION));
	// glGetString can return NULL if no rendering context has been created
	os << "GL version: ";
	if (glVer == NULL)
		os << "NULL (has RC been created?)";
	else
		os << glVer;
	os << "; GLSL version: ";
	if (glslVer == NULL)
		os << "NULL (has RC been created?)";
	else
		os << glslVer;
	os << '\n';
}

void Controller::reshapeCB(int width, int height)
{
	Controller::curController->vpWidth = width;
	Controller::curController->vpHeight = height;
	Controller::curController->doReshape();
}

void Controller::updateWCBoundingBox(ModelView* m)
{
	if (m == NULL)
		return;
	if (overallWCBoundingBoxInitialized)
	{
		float xyz[6];
		m->getWCBoundingBox(xyz);
		for (int i=0 ; i<6 ; i+=3)
		{
			if (xyz[i] < overallWCBoundingBox[i])		//update x
				overallWCBoundingBox[i] = xyz[i];
			if (xyz[i+1] > overallWCBoundingBox[i+1])	//update y
				overallWCBoundingBox[i+1] = xyz[i+1];
			if (xyz[i+2] > overallWCBoundingBox[i+2])	//update z
				overallWCBoundingBox[i+2] = xyz[i+2];
		}
	}
	else
	{
		m->getWCBoundingBox(overallWCBoundingBox);
		overallWCBoundingBoxInitialized = true;
	}
}

