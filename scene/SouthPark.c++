﻿/* 
Name: Nathan Disidore
ID: 2112424
Class: EECS 672
Project 3

// SouthPark.c++ - a concrete subclass of SouthPark that draws the south park character
*/

#include <cmath>
#include <stdio.h>

#include "SouthPark.h"
#include "ShaderIF.h"

GLuint SouthPark::shaderProgram = 0;

//Attributes to pass to shader
GLint SouthPark::aLoc_mcPosition = -1;		//modeling coordinate position
GLint SouthPark::aLoc_mcNormal = -1;		//modeling coordinate normal
GLint SouthPark::aLoc_texCoords = -1;		//texture coordinates

/* Uniforms to pass to shader */
//Matrices
GLint SouthPark::uLoc_modelViewMatrix = -1;	//Model View Matrix
GLint SouthPark::uLoc_projectionMatrix = -1;//Projection Matrix
GLint SouthPark::uLoc_normalMatrix = -1;	//Normal Matrix
//Lighting Stuff
GLint SouthPark::uLoc_ecLightPosition = -1;	//position of light source
GLint SouthPark::uLoc_lightStrength = -1;	//strength of light source RGBA
GLint SouthPark::uLoc_globalAmbient = -1;	//strength of ambient light
GLint SouthPark::uLoc_actualNumLights = -1;	//actual number of lights in scene
GLint SouthPark::uLoc_spotDir = -1;			//direction of lighting spotlight
GLint SouthPark::uLoc_spotParams = -1;		//[i][0]: cutoffAngleInDegrees (spotlight if and only if < 90); [i][1]: exponent
//Material Properties
GLint SouthPark::uLoc_kakdks = -1;			//RGBA of ambient, diffuse, specular reflection
GLint SouthPark::uLoc_m = -1;				//shininess
GLint SouthPark::uLoc_haveTextureMap = -1;	//does material use a texture?
GLint SouthPark::uLoc_textureMap = -1;		//t,s mapping of texture

//define some global colors to be used in the program
vec4 blackRGBA = { 0.0, 0.0, 0.0, 1.0 };
vec4 whiteRGBA = { 1.0, 1.0, 1.0, 1.0 };
vec4 orangeRGBA = { 1.0, 0.4, 0.0, 1.0 };
vec4 darkOrangeRGBA = {0.6, 0.2, 0.0, 1.0};
vec4 brownRGBA = {0.4, 0.2, 0.0, 1.0};
vec4 tanRGBA = {1.0, 0.8, 0.4, 1.0};
vec4 redRGBA = {1.0, 0.0, 0.0, 1.0};

vec4 genericKD = {0.4, 0.4, 0.4, 1.0};
vec4 genericKS = {1.0, 1.0, 0.6, 1.0};

float ambientStrength[] = { 0.15, 0.15, 0.15, 1.0 }; // assumed ambient light

SouthPark::SouthPark(const cryph::AffPoint origin) :
	nextManMeshIndex(0), nextSSMeshIndex(0)
{
	if (SouthPark::shaderProgram == 0)
	{
		SouthPark::shaderProgram = ShaderIF::initShader("../shaders/phong.vsh", "../shaders/phong.fsh");
	}

	// Now do instance-specific initialization:
	// Set instance origins
	modelOrigin = cryph::AffPoint(origin);

	doInitialize();
}

SouthPark::~SouthPark()
{
	//rewrite deconstructor to handle the meshes
	for (int i=0; i<nextSSMeshIndex; i++)
	{
		simpleShapeMesh* curMesh = ssMeshes[i];

		//clear the server side arrays/buffers
		glDeleteVertexArrays(1, &curMesh->vao);
		glDeleteBuffers(numAttributes, curMesh->buffer);

		if (curMesh->texImage != NULL)
			delete curMesh->texImage;
		if (curMesh->texID > 0)
			glDeleteTextures(1, &curMesh->texID);
		if (curMesh->meshShape != NULL)
			delete curMesh->meshShape;

		//delete the mesh itself
		delete [] curMesh;
		curMesh == NULL;
	}

	//handle manual meshes
	for (int i=0; i<nextManMeshIndex; i++)
	{
		manualMesh* curMesh = manMeshes[i];

		//clear the server side arrays/buffers
		glDeleteVertexArrays(1, &curMesh->vao);
		glDeleteBuffers(numAttributes, curMesh->buffer);

		//delete list of attributes if not already done so
		delete [] curMesh->vertexArray;
		delete [] curMesh->normalArray;

		//delete the mesh itself
		delete [] curMesh;
		curMesh == NULL;
	}
}


/*****************************************************************
 * PROJECT SETUP CODE
 *
 * The following are functions that setup the project
 * using the desired parameters
 ******************************************************************/

void SouthPark::doInitialize()
{
	if (SouthPark::shaderProgram > 0)
	{
		//--ATRIBUTE LOCATIONS--
		SouthPark::aLoc_mcPosition = glGetAttribLocation(shaderProgram, "mcPosition");
		SouthPark::aLoc_mcNormal = glGetAttribLocation(shaderProgram, "mcNormal");
		SouthPark::aLoc_texCoords = glGetAttribLocation(shaderProgram, "texCoords");

		//--UNIFORM LOCATIONS--
		//modeling matrixes
		SouthPark::uLoc_modelViewMatrix = glGetUniformLocation(shaderProgram, "modelViewMatrix");
		SouthPark::uLoc_normalMatrix = glGetUniformLocation(shaderProgram, "normalMatrix");
		SouthPark::uLoc_projectionMatrix = glGetUniformLocation(shaderProgram, "projectionMatrix");
		//lighting model stuff
		SouthPark::uLoc_ecLightPosition = glGetUniformLocation(shaderProgram, "ecLightPosition");
		SouthPark::uLoc_lightStrength = glGetUniformLocation(shaderProgram, "lightStrength");
		SouthPark::uLoc_globalAmbient = glGetUniformLocation(shaderProgram, "globalAmbient");
		SouthPark::uLoc_actualNumLights = glGetUniformLocation(shaderProgram, "actualNumLights");
		SouthPark::uLoc_spotDir = glGetUniformLocation(shaderProgram, "spotDir");
		SouthPark::uLoc_spotParams = glGetUniformLocation(shaderProgram, "spotParams");
		//material properties
		SouthPark::uLoc_kakdks = glGetUniformLocation(shaderProgram, "kakdks");
		SouthPark::uLoc_m = glGetUniformLocation(shaderProgram, "shininess");
		SouthPark::uLoc_haveTextureMap = glGetUniformLocation(shaderProgram, "haveTextureMap");
		SouthPark::uLoc_textureMap = glGetUniformLocation(shaderProgram, "textureMap");
	}

	SouthPark::setupVars();
	SouthPark::defineAllKenny();
}

void SouthPark::setupVars()
{
	//Boundaries
	xMin = -1; xMax = 1;
	yMin = -1; yMax = 1;
	//Z values need to be all negative for perspective
	zMin = -1; zMax = -10;

	//The eye center coordinates
	//Creating the eye center
	eyeCenter = cryph::AffPoint(0.05, 0.05, -5.01);

	//The attention point coordinates
	//now set the center of attention to the center of the model
	attentionCenter = cryph::AffPoint(modelOrigin);

	//Setting the up vector
	upVec = cryph::AffVector(0,1,0);

	//creating the unit vectors
	xUnit = cryph::AffVector(1,0,0);
	yUnit = cryph::AffVector(0,1,0);
	zUnit = cryph::AffVector(0,0,1);

	//Creating the coordinate system
	//z direction,
	wVec = eyeCenter - attentionCenter;
	distanceEyeCenter = (float)wVec.length();
	wVec.normalize();
	wVec = -wVec;

	//Getting the correct vVec coordinates from the up vector
	cryph::AffVector upPar, upPerp;
	vVec.decompose(upVec, upPar, upPerp);
	vVec = upPerp;
	vVec.normalize();

	//Getting the u vector
	uVec = vVec.cross(wVec);

	//default to orthogonal projection
	//setTmpMView();		//TMPMVIEW
	orthogonalProjection(xMin, xMax, yMin, yMax, zMin, zMax);
}

void SouthPark::defineAllKenny()
{
	defineHead();
	defineTorso();
	defineArms();
	//defineFaceSkin();
	defineFeet();
	//defineEyes();
	defineLines();
	defineHandsFingers();
}

void SouthPark::defineLightingEnvironment()
{
	/*
	Lighting info setup for the scene
	Includes:
		-ambient sources
		-directional sources
		-point sources
		-spotlights
	*/
	const int numLights = 2;
	glUniform1i(uLoc_actualNumLights, numLights);
	float lightPos[4*numLights] =
	{
		-1.0, -1.0, -7.0, 0.0, // directional
		0.7, 0.7, 0.0, 1.0   // point (at eye)
	};
	glUniform4fv(uLoc_ecLightPosition, numLights, lightPos);
	float lightStrength[4*numLights] =
	{
		0.4, 0.4, 0.4, 1.0, // 40% white
		0.7, 0.7, 0.7, 1.0  // 70% white
	};
	glUniform4fv(uLoc_lightStrength, numLights, lightStrength);
	float spotDir[3*numLights] =
	{
		0.0, 0.0, 0.0,  // these values are irrelevant since light source 0 is directional
		0.0, 0.0, -1.0  // spot light pointing along line of sight
	};
	glUniform3fv(uLoc_spotDir, numLights, spotDir);
	float spotParams[2*numLights] =
	{
		0.0, 0.0,  // these values are irrelevant since light source 0 is directional
		// uncomment one of the following two lines, depending on the desired parameters for light source 1
		//10.0, 25.0 // ten degree field of view; exponent is 25
		180.0, 0.0 // point source, but not a spot light
	};
	glUniform2fv(uLoc_spotParams, numLights, spotParams);
	
	float globalAmbient[] = { 0.15, 0.15, 0.15, 1.0 };
	glUniform4fv(uLoc_globalAmbient, 1, globalAmbient);
}

/*****************************************************************
 * GEOMETRY DEFINITIONS
 *
 * The following are functions create the meshes for various
 * objects to be rendered
 ******************************************************************/

void SouthPark::defineHead()
{
	//---The Head/Hoodie---
	//set up head coordinates/vertices
	simpleShapeMesh* head = new simpleShapeMesh;
	head->name = "Head Exterior";
	cryph::AffPoint headCenter = cryph::AffPoint(0.0,0.3,0.0);
	SouthPark::setSSMeshColor(head, orangeRGBA, orangeRGBA, genericKS);
	head->meshShape = SimpleShape::makeSphere(headCenter, 0.65, 50, 51, -0.1, 2.2, -0.2, 1.15);

	//Texture Time!
	SouthPark::setTextureImage(head, "../images/kennyFace.jpg");

	SouthPark::handleSSMeshVAO(head, headIndex);
}

void SouthPark::defineHandsFingers()
{
	// Left Hand
	simpleShapeMesh* leftHand = new simpleShapeMesh;
	cryph::AffPoint lHandC = cryph::AffPoint(-0.58, -0.683, 0.00);
	leftHand->name = "Left Hand";
	SouthPark::setSSMeshColor(leftHand, orangeRGBA, orangeRGBA, genericKS);
	leftHand->meshShape = SimpleShape::makeSphere(lHandC, 0.11, 12, 13);
	SouthPark::handleSSMeshVAO(leftHand, leftHandIndex);

	// Right Hand
	simpleShapeMesh *rightHand = new simpleShapeMesh;
	rightHand->name = "Right Hand";
	cryph::AffPoint rHandC = cryph::AffPoint(.58, -0.683, 0.00);
	SouthPark::setSSMeshColor(rightHand, orangeRGBA, orangeRGBA, genericKS);
	rightHand->meshShape = SimpleShape::makeSphere(rHandC, 0.11, 12, 13);
	SouthPark::handleSSMeshVAO(rightHand, rightHandIndex);

	// Left hand's Thumb
	simpleShapeMesh *leftThumb = new simpleShapeMesh;
	leftThumb->name ="Left Thumb";
	cryph::AffPoint lThumbC = cryph::AffPoint(-0.56, -0.66, -0.1);
	SouthPark::setSSMeshColor(leftThumb, brownRGBA, brownRGBA, genericKS);
	leftThumb->meshShape = SimpleShape::makeSphere(lThumbC, 0.05, 8, 9);
	SouthPark::handleSSMeshVAO(leftThumb, leftThumbIndex);

	// Right Thumb
	simpleShapeMesh *rightThumb = new simpleShapeMesh;
	rightThumb->name ="Right Thumb";
	cryph::AffPoint rThumbC = cryph::AffPoint(0.56, -0.66, -0.1);
	SouthPark::setSSMeshColor(rightThumb, brownRGBA, brownRGBA, genericKS);
	rightThumb->meshShape = SimpleShape::makeSphere(rThumbC, 0.05, 8, 9);
	SouthPark::handleSSMeshVAO(rightThumb, rightThumbIndex);
}

void SouthPark::defineTorso()
{
	//---The Torso/Legs---
	simpleShapeMesh *torso = new simpleShapeMesh;
	torso->name = "Torso";
	cryph::AffPoint torsoTop(0.0,-0.15,0.0), torsoBtm(0.0,-0.85,0.0);
	SouthPark::setSSMeshColor(torso, orangeRGBA, orangeRGBA, genericKS);
	torso->meshShape = SimpleShape::makeBoundedCylinder(torsoTop, torsoBtm, 0.4, 25, 4);
	SouthPark::handleSSMeshVAO(torso, torsoIndex);
}

void SouthPark::defineArms()
{
	simpleShapeMesh *leftArm = new simpleShapeMesh;
	leftArm->name = "Left Arm";
	cryph::AffPoint lArmTop(-0.3,-0.3,0.0), lArmBtm(-0.58,-0.68,0.0);
	SouthPark::setSSMeshColor(leftArm, orangeRGBA, orangeRGBA, genericKS);
	leftArm->meshShape = SimpleShape::makeBoundedCylinder(lArmTop, lArmBtm, 0.08, 25, 4);
	SouthPark::handleSSMeshVAO(leftArm, leftArmIndex);

	simpleShapeMesh *rightArm = new simpleShapeMesh;
	rightArm->name = "Right Arm";
	cryph::AffPoint rArmTop(0.3,-0.3,0.0), rArmBtm(0.58,-0.68,0.0);
	SouthPark::setSSMeshColor(rightArm, orangeRGBA, orangeRGBA, genericKS);
	rightArm->meshShape = SimpleShape::makeBoundedCylinder(rArmTop, rArmBtm, 0.08, 25, 4);
	SouthPark::handleSSMeshVAO(rightArm, rightArmIndex);
}

void SouthPark::defineLines()
{
	//---The Zipper---
	simpleShapeMesh *zipper = new simpleShapeMesh;
	zipper->name = "Zipper";
	cryph::AffPoint zipperTop(0.0,-0.15,-0.39), zipperBtm(0.0,-0.55,-0.39);
	SouthPark::setSSMeshColor(zipper, blackRGBA, blackRGBA, genericKS);
	zipper->meshShape = SimpleShape::makeBoundedCylinder(zipperTop, zipperBtm, 0.015, 25, 4);
	SouthPark::handleSSMeshVAO(zipper, zipperIndex);
}

void SouthPark::defineFeet()
{
	//---The Feet/Shoes---
	simpleShapeMesh *feet = new simpleShapeMesh;
	feet->name = "Shoes/Feet";
	cryph::AffPoint feetTop(0.0,-0.84,0.0), feetBtm(0.0,-0.88,0.0);
	SouthPark::setSSMeshColor(feet, blackRGBA, blackRGBA, genericKS);
	feet->meshShape = SimpleShape::makeBoundedCylinder(feetTop, feetBtm, 0.5, 25, 4);
	SouthPark::handleSSMeshVAO(feet, feetIndex);
}

/*****************************************************************
 * UTILITY FUNCTIONS
 *
 * The following are functions are utilities used to keep track and
 * maintain the current project
 *
 ******************************************************************/

void SouthPark::getWCBoundingBox(float* xyzLimits) const // {xmin, xmax, ymin, ymax, zmin, zmax}
{
	xyzLimits[0] = modelOrigin[0] + xMin; xyzLimits[1] = modelOrigin[0] + xMax; // (xmin, xmax)
	xyzLimits[2] = modelOrigin[1] + yMin; xyzLimits[3] = modelOrigin[1] + yMax; // (ymin, ymax)
	xyzLimits[4] = modelOrigin[2] + zMin; xyzLimits[5] = modelOrigin[2] + zMax; // (zmin, zmax)
}

void SouthPark::invertXCoord(manualMesh* oldMesh, manualMesh* newMesh)
{
	int totalVerts = oldMesh->elementCount;
	int i=0;
    newMesh->vertexArray = new vec3[totalVerts];	//vertex array
    newMesh->normalArray = new vec3[totalVerts];	//normal array
	for (i=0 ; i<totalVerts; i++)
	{
		//Copy and manipulate vertex positions
		newMesh->vertexArray[i][0] = -1.0*oldMesh->vertexArray[i][0];	//invert x
		newMesh->vertexArray[i][1] = oldMesh->vertexArray[i][1];		//leave y alone
		newMesh->vertexArray[i][2] = oldMesh->vertexArray[i][2];		//leave z alone

		//Copy and manipulate vertex normals
		newMesh->normalArray[i][0] = -1.0*oldMesh->normalArray[i][0];	//invert x normal too
		newMesh->normalArray[i][1] = oldMesh->normalArray[i][1];		//leave y alone
		newMesh->normalArray[i][2] = oldMesh->normalArray[i][2];		//leave z alone
	}
    printf("Mesh Object: %d\tName: %s\n\tInvert X Coordinate with %d vertexes\n",nextManMeshIndex,newMesh->name.c_str(),i);
	newMesh->elementCount = oldMesh->elementCount;	//same size so copy element count
}

void SouthPark::setManualMeshColor(manualMesh *mesh, vec4 ka, vec4 kd, vec4 ks)
{
	// Ambient Color
	mesh->kakdks[0] = ka[0];
	mesh->kakdks[1] = ka[1];
	mesh->kakdks[2] = ka[2];
	mesh->kakdks[3] = ka[3];

	//Diffuse Color
	mesh->kakdks[4] = kd[0];
	mesh->kakdks[5] = kd[1];
	mesh->kakdks[6] = kd[2];
	mesh->kakdks[7] = kd[3];

	//Specular Color
	mesh->kakdks[8] = ks[0];
	mesh->kakdks[9] = ks[1];
	mesh->kakdks[10] = ks[2];
	mesh->kakdks[11] = ks[3];
}

void SouthPark::setSSMeshColor(simpleShapeMesh *mesh, vec4 ka, vec4 kd, vec4 ks)
{
	// Ambient Color
	mesh->kakdks[0] = ka[0];
	mesh->kakdks[1] = ka[1];
	mesh->kakdks[2] = ka[2];
	mesh->kakdks[3] = ka[3];

	//Diffuse Color
	mesh->kakdks[4] = kd[0];
	mesh->kakdks[5] = kd[1];
	mesh->kakdks[6] = kd[2];
	mesh->kakdks[7] = kd[3];

	//Specular Color
	mesh->kakdks[8] = ks[0];
	mesh->kakdks[9] = ks[1];
	mesh->kakdks[10] = ks[2];
	mesh->kakdks[11] = ks[3];
}

void SouthPark::handleManualMeshVAO(manualMesh *mesh, int& index)
{
	//get index of next mesh object and store as a reference for later
	index = nextManMeshIndex++;
	//add the current mesh to the list of mesh pointers, so we can use it later
	manMeshes[index] = mesh;
	//bind the vao found in the current mesh
	glBindVertexArray(mesh->vao);

	glGenBuffers(numAttributes, mesh->buffer);

	// we will use glVertexAttrib in render, so:
	//glDisableVertexAttribArray(SouthPark::aLoc_vColor);

	//first handle the position attribute
	glBindBuffer(GL_ARRAY_BUFFER, mesh->buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, mesh->elementCount*sizeof(vec3), mesh->vertexArray, GL_STATIC_DRAW);
	glVertexAttribPointer(
			SouthPark::aLoc_mcPosition,
	        3, GL_FLOAT, GL_FALSE, 0,
	        0
	);
	glEnableVertexAttribArray(SouthPark::aLoc_mcPosition);

	//now handle the normal attribute
	glBindBuffer(GL_ARRAY_BUFFER, mesh->buffer[1]);
	glBufferData(GL_ARRAY_BUFFER, mesh->elementCount*sizeof(vec3), mesh->normalArray, GL_STATIC_DRAW);
	glVertexAttribPointer(
	        SouthPark::aLoc_mcNormal,
	        3, GL_FLOAT, GL_FALSE, 0,
	        0
	);
	glEnableVertexAttribArray(SouthPark::aLoc_mcNormal);
}

void SouthPark::handleSSMeshVAO(simpleShapeMesh* object, int& index)
{
	int nPoints = object->meshShape->getNumPoints();

	//get index of next mesh object and store as a reference for later
	index = nextSSMeshIndex++;
	//add the current mesh to the list of mesh pointers, so we can use it later
	ssMeshes[index] = object;
	//bind the vao found in the current mesh
	glBindVertexArray(object->vao);

	//bind the general buffers
	glGenBuffers(numAttributes+1, object->buffer);
	/*
	 * General Buffers:
	 * 		0: Vertex Buffer	[vertexBuffer]
	 * 		1: Normal Buffer	[normalBuffer]
	 * 		2: Texture Buffer 	[textureCoordBuffer]
	 */

	// There is always a vertex buffer:
	glBindBuffer(GL_ARRAY_BUFFER, object->buffer[0]);
	const float* pts = object->meshShape->getPointCoords();
	glBufferData(GL_ARRAY_BUFFER, nPoints*3*sizeof(float), pts, GL_STATIC_DRAW);
	glEnableVertexAttribArray(SouthPark::aLoc_mcPosition);
	glVertexAttribPointer(SouthPark::aLoc_mcPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// there is always a normal buffer
	glBindBuffer(GL_ARRAY_BUFFER, object->buffer[1]);
	const float* normals = object->meshShape->getNormals();
	glBufferData(GL_ARRAY_BUFFER, nPoints*3*sizeof(float), normals, GL_STATIC_DRAW);
	glEnableVertexAttribArray(SouthPark::aLoc_mcNormal);
	glVertexAttribPointer(SouthPark::aLoc_mcNormal, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// there may or may not be texture coordinates
	const float* texCoords = object->meshShape->getTextureCoords();
	if (texCoords == NULL)
		glDisableVertexAttribArray(SouthPark::aLoc_texCoords);
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, object->buffer[2]);
		glBufferData(GL_ARRAY_BUFFER, nPoints*2*sizeof(float), texCoords, GL_STATIC_DRAW);
		glEnableVertexAttribArray(SouthPark::aLoc_texCoords);
		glVertexAttribPointer(SouthPark::aLoc_texCoords, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}
}

void SouthPark::handleKey(unsigned char key)
{
	if (key == 'p')
	{
		//switch projection type
		projectionType = !projectionType;
		if (projectionType)
			perspectiveProjection(xMin, xMax, yMin, yMax, zMin, zMax, -distanceEyeCenter);
		else
			orthogonalProjection(xMin, xMax, yMin, yMax, zMin, zMax);
	}
}


void SouthPark::cleanManualMesh(manualMesh* mesh)
{
	delete [] mesh->vertexArray;
	delete [] mesh->normalArray;
	mesh->vertexArray = NULL;
	mesh->normalArray = NULL;
}

void SouthPark::render()
{
	// save the current GLSL program in use
	GLint pgm;
	glGetIntegerv(GL_CURRENT_PROGRAM, &pgm);
	
	// activate our vertex-fragment shader program
	glUseProgram(shaderProgram);

	//set up the lighting environment
	SouthPark::defineLightingEnvironment();

	//make sure the matrices to pass to OpenGL are up to date
	SouthPark::updateMatrices();

	//set up matrices for passing to shader programs
	//viewpoint uniform matrixes
	glUniformMatrix4fv(uLoc_modelViewMatrix, 1, GL_TRUE, (GLfloat*)&modelViewMatrix[0]);
	glUniformMatrix4fv(uLoc_projectionMatrix, 1, GL_TRUE, (GLfloat*)&projectionMatrix[0]);
	glUniformMatrix3fv(uLoc_normalMatrix, 1, GL_TRUE, (GLfloat*)&normalMatrix[0]);

	// Draw Manual Meshes
	for (int i=0 ; i<nextManMeshIndex; i++)
	{
		manualMesh *curMesh = manMeshes[i];
		drawManualMesh(curMesh);
	}
	
	// Draw Simple Shape Meshes
	for (int i=0; i<nextSSMeshIndex; i++)
	{
		simpleShapeMesh* curMesh = ssMeshes[i];
		drawSimpleShape(curMesh);
	}

	// Done drawing; restore the previous program
	if (pgm > 0)
		glUseProgram(pgm);
}

void SouthPark::drawManualMesh(manualMesh* object)
{
	glBindVertexArray(object->vao);

	// ambient, diffuse, and specular reflectances along with specular exponent
	glUniform4fv(uLoc_kakdks, 3, object->kakdks);
	glUniform1f (uLoc_m, 20.0); 				//Shininess
	glUniform1i(uLoc_haveTextureMap, 0);		//No texture Map

	glEnableVertexAttribArray(SouthPark::aLoc_mcNormal);	//Enable normal array

	glDrawArrays(object->drawMode, 0, object->elementCount);

	/*//draw outline too if it's the thumbs or head
	if ((i == rightThumbIndex) || (i == leftThumbIndex) || (i == headIndex))
	{
		//glVertexAttrib4fv(SouthPark::aLoc_vColor, blackRGBA);
		glDrawArrays(GL_LINE_LOOP, 0, object->elementCount);
	}*/
}

void SouthPark::drawSimpleShape(simpleShapeMesh* object)
{
	/*
	Handles drawing of object given geometries defined
	in a pointer to a simpleshape class.
	Taken and modified from the SimpleShapeViewer class.
	*/
	if (object->meshShape == NULL)
		return;

	//bind object VAO
	glBindVertexArray(object->vao);

	// set the shape's material properties:
	// ambient, diffuse, and specular reflectances along with specular exponent
	glUniform4fv(uLoc_kakdks, 3, object->kakdks);
	glUniform1f (uLoc_m, 20.0);
	
	// draw the curved surface part
	// first see if a texture is to be applied
	if (object->texImage == NULL)
		glUniform1i(uLoc_haveTextureMap, 0);
	else
	{
		glUniform1i(uLoc_haveTextureMap, 1);
		glUniform1i(uLoc_textureMap, 0); // refers to '0' in glActiveTexture(GL_TEXTURE0);
	}

	GLenum mode;
	int offset;
	int nPoints = object->meshShape->getDrawArraysData(mode, offset);
	if (nPoints > 0)
	{
		glEnableVertexAttribArray(SouthPark::aLoc_mcNormal);
		glDrawArrays(mode, offset, nPoints);
	}

	// SimpleShape may also have index list data to be drawn (e.g., caps, if present;
	// also index lists are used if nPointsAlongAxis > 2).
	int nInList;
	GLenum type;
	cryph::AffVector fixedN;
	float normal[3];
	bool canUseTexCoordArray, canUseNormalArray;
	for (int i=0 ; i < object->meshShape->getNumIndexLists() ; i++)
	{
		const void* indices = object->meshShape->getIndexList(i, mode, nInList, type,
									canUseTexCoordArray, canUseNormalArray, fixedN);
		if (canUseNormalArray)
			glEnableVertexAttribArray(SouthPark::aLoc_mcNormal);
		else
		{
			glDisableVertexAttribArray(SouthPark::aLoc_mcNormal);
			fixedN.vComponents(normal);
			glVertexAttrib3fv(SouthPark::aLoc_mcNormal, normal);
		}
		if (canUseTexCoordArray)
		{
			// first see if a texture is to be applied
			if (object->texImage == NULL)
				glUniform1i(uLoc_haveTextureMap, 0);
			else
			{
				glUniform1i(uLoc_haveTextureMap, 1);
				glBindTexture(GL_TEXTURE_2D, object->texID);
				glUniform1i(uLoc_textureMap, 0); // refers to texture unit '0', the only one this program uses
			}
		}
		else
			glUniform1i(uLoc_haveTextureMap, 0);
		glDrawElements(mode, nInList, type, indices);
	}
}

/*****************************************************************
 * SHAPE GEOMETRIES
 *
 * The following are functions used to generated the vertices of
 * shapes used in various parts of the project
 ******************************************************************/

void SouthPark::genSphere(const vec3 center, float r, int p, manualMesh* mesh) {
	/*
	 * Generates a 3D sphere of radius "r" with center "center"
	 * Modified from base taken from:
	 *    https://gist.github.com/1076642
	 */
    const int totalVerts = (p+1)*2*(p/2);
    mesh->vertexArray = new vec3[totalVerts];	//vertex array
    mesh->normalArray = new vec3[totalVerts];	//normal array
    float theta1 = 0.0, theta2 = 0.0, theta3 = 0.0;
    float ex = 0.0f, ey = 0.0f, ez = 0.0f;
    float px = 0.0f, py = 0.0f, pz = 0.0f;

    if( r < 0 )
        r = -r;

    if( p < 0 )
        p = -p;

    int counter=0;
    for(int i = 0; i < p/2; ++i)
    {
        theta1 = i * (M_PI*2) / p - M_PI_2;
        theta2 = (i + 1) * (M_PI*2) / p - M_PI_2;
        for(int j = 0; j <= p; ++j)
        {
            theta3 = j * (M_PI*2) / p;

            //vertex one coordinate generation
            ex = cos(theta2) * cos(theta3);
            ey = sin(theta2);
            ez = cos(theta2) * sin(theta3);
            //offset and scale not needed for normal calculation
            //so normal components are just e(x|y|z)
            px = center[0] + r * ex;
            py = center[1] + r * ey;
            pz = center[2] + r * ez;

            mesh->vertexArray[counter][0] = px;
            mesh->vertexArray[counter][1] = py;
            mesh->vertexArray[counter][2] = pz;
            mesh->normalArray[counter][0] = ex;
            mesh->normalArray[counter][1] = ey;
            mesh->normalArray[counter][2] = ez;
            //mesh->textureArray[counter][0] = -(j/(float)p);
            //mesh->textureArray[counter][1] = 2*(i+1)/(float)p;
            counter++;

            //SECONDARY VERTEX
            ex = cos(theta1) * cos(theta3);
            ey = sin(theta1);
            ez = cos(theta1) * sin(theta3);
            px = center[0] + r * ex;
            py = center[1] + r * ey;
            pz = center[2] + r * ez;

            mesh->vertexArray[counter][0] = px;
            mesh->vertexArray[counter][1] = py;
            mesh->vertexArray[counter][2] = pz;
            mesh->normalArray[counter][0] = ex;
            mesh->normalArray[counter][1] = ey;
            mesh->normalArray[counter][2] = ez;
            //mesh->textureArray[counter][0] = -(j/(float)p);
            //mesh->textureArray[counter][1] = 2*i/(float)p;
            counter++;
        }
    }
    mesh->elementCount = counter;
    printf("Mesh Object: %d\tName: %s\n\tSphere Generated with %d vertexes\n",nextManMeshIndex,mesh->name.c_str(),counter);
}

void SouthPark::genCylinder(const vec3 center, float radius, float length, float slices, manualMesh* mesh)
{
    const int totalVerts = (slices)*2;
    mesh->vertexArray = new vec3[totalVerts];	//vertex array
    mesh->normalArray = new vec3[totalVerts];	//normal array
	float halfLength = length/2.0;
	int counter = 0;
	for(int i=0; i<slices; i++) {
		float theta = ((float)(i/slices))*2.0*M_PI;

		/*vertices and normals at upperEdges of circle*/
		mesh->vertexArray[counter][0] = center[0]+radius*cos(theta);
		mesh->vertexArray[counter][1] = center[1]+halfLength;
		mesh->vertexArray[counter][2] = center[2]+radius*sin(theta);
		mesh->normalArray[counter][0] = cos(theta);
		mesh->normalArray[counter][1] = halfLength;
		mesh->normalArray[counter][0] = sin(theta);
		counter++;

		/* the same vertices and normals at the bottom of the cylinder
		   only change is halfLength is negative*/
		mesh->vertexArray[counter][0] = center[0]+radius*cos(theta);
		mesh->vertexArray[counter][1] = center[1]-halfLength;
		mesh->vertexArray[counter][2] = center[2]+radius*sin(theta);
		mesh->normalArray[counter][0] = cos(theta);
		mesh->normalArray[counter][1] = -halfLength;
		mesh->normalArray[counter][0] = sin(theta);
		counter++;
	}
	printf("Mesh Object: %d\tName: %s\n\tRegular Cylinder Generated with %d vertexes\n",nextManMeshIndex,mesh->name.c_str(),counter);
    mesh->elementCount = counter;
}

void SouthPark::genDiskCylinder(const vec3 center, const vec3 offset, int numDisks, float radius, float slices, manualMesh* mesh)
{
	const int totalVerts = numDisks*slices*2;
    mesh->vertexArray = new vec3[totalVerts];	//vertex array
    mesh->normalArray = new vec3[totalVerts];	//normal array
	int counter = 0;
	//printf("Center: \tX: %f\tY: %f\tZ: %f\n",center[0],center[1],center[2]);
	//printf("Offset: \tX: %f\tY: %f\tZ: %f\n",offset[0],offset[1],offset[2]);
	for(int diskIter=0; diskIter<numDisks; diskIter++) {
		for(int i=0; i<slices; i++) {
			float theta = ((float)(i/slices))*2.0*M_PI;

			/*vertices and normals at upperEdges of circle*/
			mesh->vertexArray[counter][0] = center[0]+diskIter*(offset[0]/-2.0)+radius*cos(theta);
			mesh->vertexArray[counter][1] = center[1]+diskIter*(offset[1]/-2.0);
			mesh->vertexArray[counter][2] = center[2]+diskIter*(offset[2]/-2.0)+radius*sin(theta);
			mesh->normalArray[counter][0] = cos(theta);
			mesh->normalArray[counter][1] = diskIter*(offset[1]/-2.0);
			mesh->normalArray[counter][0] = sin(theta);
			//if (diskIter == 1)
			//	printf("DI: %d\tC: %d\t0: %f\tX: %f\tY: %f\tZ: %f\n",diskIter,counter,theta,center[0]+diskIter*(offset[0]/-2.0)+radius*cos(theta), center[1]+diskIter*(offset[1]/-2.0), center[2]+diskIter*(offset[2]/-2.0)+radius*sin(theta));
			counter++;

			/* the same vertices and normals at the bottom of the cylinder
			   only change is halfLength is negative*/
			mesh->vertexArray[counter][0] = center[0]+diskIter*(offset[0]/2.0)+radius*cos(theta);
			mesh->vertexArray[counter][1] = center[1]+diskIter*(offset[1]/2.0);
			mesh->vertexArray[counter][2] = center[2]+diskIter*(offset[2]/2.0)+radius*sin(theta);
			mesh->normalArray[counter][0] = cos(theta);
			mesh->normalArray[counter][1] = diskIter*(offset[1]/2.0);
			mesh->normalArray[counter][0] = sin(theta);
			counter++;
		}
	}
	printf("Mesh Object: %d\tName: %s\n\tDisk Cylinder Generated with %d vertexes\n",nextManMeshIndex,mesh->name.c_str(),counter);
	mesh->elementCount = counter;
}

void SouthPark::pwlEllipse(const vec3 center, GLfloat xR, GLfloat yR, int numVerts, manualMesh* mesh) // CLASS METHOD
{
	const int totalVerts = numVerts;
    mesh->vertexArray = new vec3[totalVerts];	//vertex array
    mesh->normalArray = new vec3[totalVerts];	//normal array
	if (numVerts < 4)
		numVerts = 4;
	GLfloat dTheta = 2.0*M_PI/static_cast<GLfloat>(numVerts);
	GLfloat theta = 0.0;
	for (int i=0 ; i<numVerts ; i++)
	{
		mesh->vertexArray[i][0] = modelOrigin[0] + center[0] + xR * cos(theta);
		mesh->vertexArray[i][1] = modelOrigin[1] + center[1] + yR * sin(theta);
		mesh->vertexArray[i][2] = modelOrigin[2] + center[2];	//keep z still

		mesh->normalArray[i][0] = 1.0;
		mesh->normalArray[i][1] = 0.0;
		mesh->normalArray[i][2] = 0.0;
		theta += dTheta;
	}
	mesh->elementCount = numVerts;
	printf("Mesh Object: %d\tName: %s\n\tPWL Ellipse Generated with %d vertexes\n",nextManMeshIndex,mesh->name.c_str(),numVerts);
}

void SouthPark::genEllipse(const vec3 center, float xR, float yR, float zR, int p, manualMesh* mesh)
{
	/*
	 * Generates a 3D sphere of radius "r" with center "center"
	 * Modified from base taken from:
	 *    https://gist.github.com/1076642
	 */
    const int totalVerts = (p+1)*2*(p/2);
    mesh->vertexArray = new vec3[totalVerts];	//vertex array
    mesh->normalArray = new vec3[totalVerts];	//normal array
    float theta1 = 0.0, theta2 = 0.0, theta3 = 0.0;
    float ex = 0.0f, ey = 0.0f, ez = 0.0f;
    float px = 0.0f, py = 0.0f, pz = 0.0f;

    if( p < 0 )
        p = -p;

    int counter=0;
    for(int i = 0; i < p/2; ++i)
    {
        theta1 = i * (M_PI*2) / p - M_PI_2;
        theta2 = (i + 1) * (M_PI*2) / p - M_PI_2;
        for(int j = 0; j <= p; ++j)
        {
            theta3 = j * (M_PI*2) / p;

            //vertex one coordinate generation
            ex = cos(theta2) * cos(theta3);
            ey = sin(theta2);
            ez = cos(theta2) * sin(theta3);
            //offset and scale not needed for normal calculation
            //so normal components are just e(x|y|z)
            px = center[0] + xR * ex;
            py = center[1] + yR * ey;
            pz = center[2] + zR * ez;

            mesh->vertexArray[counter][0] = px;
            mesh->vertexArray[counter][1] = py;
            mesh->vertexArray[counter][2] = pz;
            mesh->normalArray[counter][0] = ex;
            mesh->normalArray[counter][1] = ey;
            mesh->normalArray[counter][2] = ez;
            //mesh->textureArray[counter][0] = -(j/(float)p);
            //mesh->textureArray[counter][1] = 2*(i+1)/(float)p;
            counter++;

            //SECONDARY VERTEX
            ex = cos(theta1) * cos(theta3);
            ey = sin(theta1);
            ez = cos(theta1) * sin(theta3);
            px = center[0] + xR * ex;
            py = center[1] + yR * ey;
            pz = center[2] + zR * ez;

            mesh->vertexArray[counter][0] = px;
            mesh->vertexArray[counter][1] = py;
            mesh->vertexArray[counter][2] = pz;
            mesh->normalArray[counter][0] = ex;
            mesh->normalArray[counter][1] = ey;
            mesh->normalArray[counter][2] = ez;
            //mesh->textureArray[counter][0] = -(j/(float)p);
            //mesh->textureArray[counter][1] = 2*i/(float)p;
            counter++;
        }
    }
    mesh->elementCount = counter;
    printf("Mesh Object: %d\tName: %s\n\tEllipse Generated with %d vertexes\n",nextManMeshIndex,mesh->name.c_str(),counter);
}

/******************************
 * Texture Code Snippets 
 *
 * Following code snippets handle textures
 * for an object in the scene
 *
 ******************************/
void SouthPark::setTextureImage(simpleShapeMesh* object, const char* imgFileName)
{
	/* 
	sets the texture image for the current mesh
	takes file name as argument
	from SimpleShapeViewer
	*/
	if (imgFileName == NULL)
		return;
	object->texImage = ImageReader::create(imgFileName);
	defineTexture(object);
}

void SouthPark::defineTexture(simpleShapeMesh* object)
{
	/*
	Sets up necessary texture info for the SimpleShape pointer
	Only called for objects that have a texture
	*/
	if (object->texImage == NULL)
		return;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &object->texID);
	glBindTexture(GL_TEXTURE_2D, object->texID);
	float white[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, white);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	GLint level = 0;
	int pw = object->texImage->getWidth(), ph = object->texImage->getHeight();
	GLenum iFormat = object->texImage->getInternalFormat();
	GLenum format = object->texImage->getFormat();
	GLenum type = object->texImage->getType();
	const GLint border = 0; // must be zero (only present for backwards compatibility)
	const GLvoid* pixelData = object->texImage->getTexture();
	glTexImage2D(GL_TEXTURE_2D, level, iFormat, pw, ph, border, format, type, pixelData);
}
