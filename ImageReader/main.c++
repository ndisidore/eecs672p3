// main.c++: A quick test program to show how to create and query an image file

#include <iostream>
using namespace std;
#include "ImageReader.h"

int main(int argc, char* argv[])
{
	for (int i=1 ; i<argc ; i++)
	{
		ImageReader* img = ImageReader::create(argv[i]);
		if (img == NULL)
		{
			cerr << "Could not create an image file for: " << argv[i] << endl;
			continue;
		}
		int pw = img->getPixelWidth(), ph = img->getPixelHeight();
		cout << "Image on file " << argv[i] << " has resolution (rows,cols) = ("
			 << ph << "," << pw << ")\n";
	}
}