// SimpleShapeViewer.h

#ifndef SIMPLESHAPEVIEWER_H
#define SIMPLESHAPEVIEWER_H

#ifdef USE_MAC_FRAMEWORKS
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include "ModelView.h"
#include "SimpleShape.h"
#include "ImageReader.h"

class SimpleShapeViewer : public ModelView
{
public:
	SimpleShapeViewer();
	virtual ~SimpleShapeViewer();

	void getWCBoundingBox(float* xyzLimits) const;
	void render();
	void setTextureImage(const char* imgFileName);

private:
	void defineGeometry();
	void defineLightingEnvironment();
	void defineTexture();
	void doInitialize();

	void drawShape(const SimpleShape* aShape);

	static GLuint shaderProgram;
	static GLint aLoc_mcPosition, aLoc_mcNormal, aLoc_texCoords;
	static GLint uLoc_actualNumLights;
	static GLint uLoc_ecLightPosition, uLoc_lightStrength,
	             uLoc_spotDir, uLoc_spotParams, uLoc_globalAmbient;
	static GLint uLoc_modelViewMatrix, uLoc_projectionMatrix, uLoc_normalMatrix;
	static GLint uLoc_kakdks, uLoc_shininess;
	static GLint uLoc_haveTextureMap, uLoc_textureMap;

	SimpleShape* theShape;
	GLuint vao, vertexBuffer, normalBuffer, textureCoordBuffer;
	ImageReader* texImage;
	GLuint texID;
};

#endif