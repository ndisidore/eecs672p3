// SimpleShape.c++ - Interface for common shapes in OpenGL

#include <math.h>

#include "SimpleShape.h"

SimpleShape::SimpleShape() : drawArraysMode(0), hasDrawArraysData(false),
	pointCoords(NULL), normals(NULL), textureCoords(NULL),
	nPoints(0), indexLists(NULL), nIndexLists(0), nIndexListsConstructed(0)
{
}

SimpleShape::~SimpleShape()
{
	if (pointCoords != NULL)
		delete [] pointCoords;
	if (normals != NULL)
		delete [] normals;
	if (textureCoords != NULL)
		delete [] textureCoords;
	if (indexLists != NULL)
	{
		for (int i=0 ; i<nIndexLists ; i++)
			if (indexLists[i].indices != NULL)
				delete[] indexLists[i].indices;
		delete [] indexLists;
	}
}

// private methods to do the actual construction
void SimpleShape::addCaps(Caps capSpec, const cryph::AffVector& axis, int nPointsAlongAxis)
{
	if (capSpec == CAP_AT_NEITHER)
		return;
	int commonIndexListLength = nPoints/2; // half the points belong in each cap
	if ((capSpec == CAP_AT_BOTTOM) || (capSpec == CAP_AT_BOTH))
	{
		indexLists[nIndexListsConstructed].mode = GL_TRIANGLE_FAN;
		indexLists[nIndexListsConstructed].indices = new int[commonIndexListLength];
		indexLists[nIndexListsConstructed].nIndices = commonIndexListLength;
		indexLists[nIndexListsConstructed].usePerVertexTexCoords = false;
		indexLists[nIndexListsConstructed].usePerVertexNormals = false;
		indexLists[nIndexListsConstructed].useFixedNormal = -axis;
		// bottom cap:
		for (int i=0 ; i<commonIndexListLength ; i++)
			indexLists[nIndexListsConstructed].indices[i] = i*nPointsAlongAxis;
		nIndexListsConstructed++;
	}
	if ((capSpec == CAP_AT_TOP) || (capSpec == CAP_AT_BOTH))
	{
		indexLists[nIndexListsConstructed].mode = GL_TRIANGLE_FAN;
		indexLists[nIndexListsConstructed].indices = new int[commonIndexListLength];
		indexLists[nIndexListsConstructed].nIndices = commonIndexListLength;
		indexLists[nIndexListsConstructed].usePerVertexTexCoords = false;
		indexLists[nIndexListsConstructed].usePerVertexNormals = false;
		indexLists[nIndexListsConstructed].useFixedNormal = axis;
		// top cap:
		for (int i=0 ; i<commonIndexListLength ; i++)
			indexLists[nIndexListsConstructed].indices[i] = (i+1)*nPointsAlongAxis - 1;
		nIndexListsConstructed++;
	}
}

void SimpleShape::allocateIndexListsForConeCyl(int nPointsAlongAxis, Caps capSpec)
{
	nIndexLists = nIndexListsConstructed = 0;
	if (nPointsAlongAxis > 2)
		nIndexLists = nPointsAlongAxis - 1;
	if (capSpec == CAP_AT_BOTH)
		nIndexLists += 2;
	else if (capSpec != CAP_AT_NEITHER)
		nIndexLists++;
	if (nIndexLists > 0)
		indexLists = new IndexListData[nIndexLists];
}

void SimpleShape::allocateIndexListsForSphere(int nPointsAlongAxis)
{
	nIndexListsConstructed = 0;
	nIndexLists = nPointsAlongAxis - 1;
	indexLists = new IndexListData[nIndexLists];
}

// Accessors for use during actual drawing during display callbacks
// May have data for a glDrawArrays call:
int SimpleShape::getDrawArraysData(GLenum& mode, int& offset) const
{
	if (hasDrawArraysData)
	{
		mode = drawArraysMode;
		offset = 0;
		return nPoints;
	}
	return 0;
}

const void* SimpleShape::getIndexList(int i, GLenum& mode, int& nInList, GLenum& type,
	bool& canUsePerVertexTexCoords, bool& canUsePerVertexNormals, cryph::AffVector& fixedNormal) const
{
	if ((indexLists != NULL) && (i < nIndexLists))
	{
		mode = indexLists[i].mode;
		nInList = indexLists[i].nIndices;
		type = GL_UNSIGNED_INT;
		canUsePerVertexTexCoords = indexLists[i].usePerVertexTexCoords;
		canUsePerVertexNormals = indexLists[i].usePerVertexNormals;
		fixedNormal = indexLists[i].useFixedNormal;
		return indexLists[i].indices;
	}
	// bad request or no index lists
	return NULL;
}

// Factory methods for creation of shapes:
// Texture coordinates are generated only if BOTH sMax>sMin AND tMax>tMin;
// 's' direction is around the circumference of the cylinder/cone; 't' direction
// is along the axis.
SimpleShape* SimpleShape::makeBoundedCone(
		const cryph::AffPoint& Pbottom, const cryph::AffPoint& Ptop,
		double radiusAtBottom, double radiusAtTop,
		int nPointsAroundSide, int nPointsAlongAxis,
		Caps capSpec, double sMin, double sMax, double tMin, double tMax,
		const cryph::AffVector& sZero)
{
	if ((nPointsAroundSide < 3) || (nPointsAlongAxis < 2) || (radiusAtBottom <= 0) || (radiusAtTop <= 0))
		return NULL;
	cryph::AffVector axis = Ptop - Pbottom;
	double height = axis.normalize();
	if (height < cryph::BasicDistanceTol)
		return NULL;
	SimpleShape* ss = new SimpleShape();
	ss->makeRuledSurfaceBetweenCircles(
			Pbottom, axis, height, radiusAtBottom, radiusAtTop,
			nPointsAroundSide, nPointsAlongAxis, capSpec, sMin, sMax, tMin, tMax, sZero);
	return ss;
}
	
SimpleShape* SimpleShape::makeBoundedCylinder(
		const cryph::AffPoint& Pbottom, const cryph::AffPoint& Ptop,
		double radius,
		int nPointsAroundSide, int nPointsAlongAxis,
		Caps capSpec,
		double sMin, double sMax, double tMin, double tMax,
		const cryph::AffVector& sZero)
{
	if ((nPointsAroundSide < 3) || (nPointsAlongAxis < 2) || (radius <= 0))
		return NULL;
	cryph::AffVector axis = Ptop - Pbottom;
	double height = axis.normalize();
	if (height < cryph::BasicDistanceTol)
		return NULL;
	SimpleShape* ss = new SimpleShape();
	ss->makeRuledSurfaceBetweenCircles(
			Pbottom, axis, height, radius, radius,
			nPointsAroundSide, nPointsAlongAxis, capSpec, sMin, sMax, tMin, tMax, sZero);
	return ss;
}

// Worker routines

void SimpleShape::makeIndexLists(int nPointsAroundSide, int nPointsAlongAxis)
{
	int commonIndexListLength = 2 * (nPointsAroundSide + 1); // '2' because two points on adjacent circular cross sections per spot around side
	int firstIndex = 0;
	for (int i=1 ; i<nPointsAlongAxis ; i++) // for each index list:
	{
		indexLists[nIndexListsConstructed].mode = GL_TRIANGLE_STRIP;
		indexLists[nIndexListsConstructed].indices = new int[commonIndexListLength];
		indexLists[nIndexListsConstructed].nIndices = commonIndexListLength;
		indexLists[nIndexListsConstructed].usePerVertexTexCoords = true;
		indexLists[nIndexListsConstructed].usePerVertexNormals = true;
		// bottom cap has the points with even numbered indices
		int delta = 0;
		for (int j=0 ; j<=2*nPointsAroundSide ; j+=2) // "<=" here so that the copied last column gets its own indices
		{
			// Since vertices are generated along a ruling, corresponding points between
			// two consecutive circular cross sections are one apart in the point array:
			indexLists[nIndexListsConstructed].indices[j  ] = firstIndex + delta;
			indexLists[nIndexListsConstructed].indices[j+1] = firstIndex + delta + 1;
			// then moving from one ruling to the next, we skip over "nPointsAlongAxis"
			// points to get to the next point on a given circular cross section:
			delta += nPointsAlongAxis;
		}
		// done with this index list
		nIndexListsConstructed++;
		// then to get to start of next index list (again, because vertices are generated
		// along a ruling):
		firstIndex++;
	}
}

void SimpleShape::makeRuledSurfaceBetweenCircles(
		const cryph::AffPoint& Pbottom, cryph::AffVector& axis,
		double height, double radiusAtBottom, double radiusAtTop,
		int nPointsAroundSide, int nPointsAlongAxis,
		Caps capSpec, double sMin, double sMax, double tMin, double tMax,
		const cryph::AffVector& sZero)
{
	nPoints = nPointsAlongAxis * (nPointsAroundSide + 1); // "+1" so that lastCol==firstCol
	pointCoords = new float[nPoints * 3]; // "*3": (x, y, z)
	normals = new float[nPoints * 3]; // "*3": (dx, dy, dz)
	// height and radius deltas along the axis
	double dh = height / (nPointsAlongAxis - 1);
	double dr = (radiusAtTop - radiusAtBottom) / (nPointsAlongAxis - 1);
	// texture parameters, if generating texture coordinates
	double s, ds, dt;
	if ((sMax > sMin) && (tMax > tMin))
	{
		s = sMin;
		ds = (sMax - sMin) / nPointsAroundSide;
		dt = (tMax - tMin) / (nPointsAlongAxis - 1);
		textureCoords = new float[nPoints * 2]; // "*2": (s, t)
	}
	cryph::AffVector u(sZero), v;
	cryph::AffVector::coordinateSystemFromUW(u, v, axis);
	double theta = 0.0, dTheta = 2.0*M_PI/nPointsAroundSide;
	int loc2 = 0, loc3 = 0;
	for (int i=0 ; i<nPointsAroundSide ; i++)
	{
		cryph::AffVector vecAwayFromAxis = cos(theta)*u + sin(theta)*v;
		theta += dTheta;
		// compute common normal along axis
		cryph::AffPoint pb = Pbottom + radiusAtBottom*vecAwayFromAxis;
		cryph::AffPoint pt = Pbottom + height*axis + radiusAtTop*vecAwayFromAxis;
		cryph::AffVector ruling = pt - pb;
		cryph::AffVector vPar, nHat;
		ruling.decompose(axis, vPar, nHat);
		if (nHat.normalize() < cryph::BasicUnitTol)
			// two radii must be (nearly) the same; normal is vecAwayFromAxis
			nHat = vecAwayFromAxis;
		// initialize parameters for points along this ruling
		double t = tMin, h = 0.0, r = radiusAtBottom;
		for (int j=0 ; j<nPointsAlongAxis ; j++)
		{
			cryph::AffPoint pnt = Pbottom + h*axis + r*vecAwayFromAxis;
			pnt.aCoords(pointCoords, loc3);
			nHat.vComponents(normals, loc3);
			loc3 += 3;
			if (textureCoords != NULL)
			{
				textureCoords[loc2  ] = s;
				textureCoords[loc2+1] = t;
				loc2 += 2;
				t += dt;
			}
			h += dh; r += dr;
		}
		s += ds;
	}
	// make lastCol == firstCol
	for (int j=0 ; j<nPointsAlongAxis*3 ; j++) // '*3' because we are copying (x,y,z) at each point
	{
		pointCoords[loc3+j] = pointCoords[j];
		normals[loc3+j] = normals[j];
	}
	if (textureCoords != NULL)
	{
		for (int j=0 ; j<nPointsAlongAxis ; j++)
		{
			// all 's' values need to be sMax:
			textureCoords[loc2  ] = sMax;
			// all 't' values need to be a copy of first column:
			textureCoords[loc2+1] = textureCoords[2*j+1];
			loc2 += 2;
		}
	}
	allocateIndexListsForConeCyl(nPointsAlongAxis, capSpec);
	hasDrawArraysData = (nPointsAlongAxis == 2);
	if (hasDrawArraysData)
		drawArraysMode = GL_TRIANGLE_STRIP;
	else
		makeIndexLists(nPointsAroundSide, nPointsAlongAxis);
	addCaps(capSpec, axis, nPointsAlongAxis);
}

SimpleShape* SimpleShape::makeSphere(const cryph::AffPoint& center, double radius,
									 int nPointsAroundSide, int nPointsAlongAxis,
									 double sMin, double sMax, double tMin, double tMax,
									 const cryph::AffVector& upAxis, const cryph::AffVector& sZero)
{
	if ((radius <= 0.0) || (nPointsAroundSide < 3) || (nPointsAlongAxis < 3))
		return NULL;
	cryph::AffVector u(sZero), v, w(upAxis);
	cryph::AffVector::coordinateSystemFromUW(u, v, w);
	SimpleShape* ss = new SimpleShape();
	ss->makeSphere(center, u, v, upAxis, radius,
				   nPointsAroundSide, nPointsAlongAxis, sMin, sMax, tMin, tMax);
	return ss;
}

void SimpleShape::makeSphere(const cryph::AffPoint& center,
			const cryph::AffVector& u, const cryph::AffVector& v, const cryph::AffVector& w,
			double radius, int nPointsAroundSide, int nPointsAlongAxis,
			double sMin, double sMax, double tMin, double tMax)
{
	nPoints = nPointsAlongAxis * (nPointsAroundSide + 1); // "+1" so that lastCol==firstCol
	pointCoords = new float[nPoints * 3]; // "*3": (x, y, z)
	normals = new float[nPoints * 3]; // "*3": (dx, dy, dz)
	// height and radius deltas along the axis
	const double maxPhi = 0.995*(M_PI/2.0); // don't go quite up to the pole
	const double minPhi = -maxPhi;
	double dPhi = (maxPhi - minPhi) / (nPointsAlongAxis - 1);
	// texture parameters, if generating texture coordinates
	double s, ds, dt;
	if ((sMax > sMin) && (tMax > tMin))
	{
		s = sMin;
		ds = (sMax - sMin) / nPointsAroundSide;
		dt = (tMax - tMin) / (nPointsAlongAxis - 1);
		textureCoords = new float[nPoints * 2]; // "*2": (s, t)
	}

	double theta = 0.0, dTheta = 2.0*M_PI/nPointsAroundSide;
	int loc2 = 0, loc3 = 0;
	for (int i=0 ; i<nPointsAroundSide ; i++)
	{
		// initialize parameters for points along this ruling
		double t = tMin;
		double phi = minPhi;
		for (int j=0 ; j<nPointsAlongAxis ; j++)
		{
			cryph::AffVector nHat = cos(phi)*(cos(theta)*u + sin(theta)*v) + sin(phi)*w;
			cryph::AffPoint pnt = center + radius*nHat;
			pnt.aCoords(pointCoords, loc3);
			nHat.vComponents(normals, loc3);
			loc3 += 3;
			if (textureCoords != NULL)
			{
				textureCoords[loc2  ] = s;
				textureCoords[loc2+1] = t;
				loc2 += 2;
				t += dt;
			}
			phi += dPhi;
		}
		theta += dTheta;
		s += ds;
	}
	// make lastCol == firstCol
	for (int j=0 ; j<nPointsAlongAxis*3 ; j++) // '*3' because we are copying (x,y,z) at each point
	{
		pointCoords[loc3+j] = pointCoords[j];
		normals[loc3+j] = normals[j];
	}
	if (textureCoords != NULL)
	{
		for (int j=0 ; j<nPointsAlongAxis ; j++)
		{
			// all 's' values need to be sMax:
			textureCoords[loc2  ] = sMax;
			// all 't' values need to be a copy of first column:
			textureCoords[loc2+1] = textureCoords[2*j+1];
			loc2 += 2;
		}
	}
	allocateIndexListsForSphere(nPointsAlongAxis);
	makeIndexLists(nPointsAroundSide, nPointsAlongAxis);
}