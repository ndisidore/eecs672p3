// SimpleShapeViewer.c++

#include "SimpleShapeViewer.h"
#include "ShaderIF.h"

// shader program id and locations of attribute and uniform variables inside it
GLuint SimpleShapeViewer::shaderProgram = 0;
GLint SimpleShapeViewer::aLoc_mcPosition = -1, SimpleShapeViewer::aLoc_mcNormal = -1,
	SimpleShapeViewer::aLoc_texCoords = -1;
GLint SimpleShapeViewer::uLoc_actualNumLights = -1;
GLint SimpleShapeViewer::uLoc_ecLightPosition = -1, SimpleShapeViewer::uLoc_lightStrength = -1,
SimpleShapeViewer::uLoc_spotDir = -1, SimpleShapeViewer::uLoc_spotParams = -1,
	SimpleShapeViewer::uLoc_globalAmbient = -1;
GLint SimpleShapeViewer::uLoc_modelViewMatrix = -1, SimpleShapeViewer::uLoc_projectionMatrix = -1,
	SimpleShapeViewer::uLoc_normalMatrix = -1;
GLint SimpleShapeViewer::uLoc_kakdks = -1, SimpleShapeViewer::uLoc_shininess = -1;
GLint SimpleShapeViewer::uLoc_haveTextureMap = -1, SimpleShapeViewer::uLoc_textureMap = -1;

SimpleShapeViewer::SimpleShapeViewer() : theShape(NULL), vertexBuffer(0), normalBuffer(0),
	textureCoordBuffer(0), texImage(NULL), texID(0)
{
	if (SimpleShapeViewer::shaderProgram == 0)
		// The first time an instance of this class is generated, create the common shader program:
#ifdef __APPLE__
		SimpleShapeViewer::shaderProgram = ShaderIF::initShader("../shaders/phong_120_SpotLight.vsh", "../shaders/simpleColorPlusTexture_120.fsh");
#else
		SimpleShapeViewer::shaderProgram = ShaderIF::initShader("../shaders/phong_330_SpotLight.vsh", "../shaders/simpleColorPlusTexture_330.fsh");
#endif
	
	// Now do instance-specific initialization:
	doInitialize();
}

SimpleShapeViewer::~SimpleShapeViewer()
{
	if (vertexBuffer > 0)
		glDeleteBuffers(1, &vertexBuffer);
	if (normalBuffer > 0)
		glDeleteBuffers(1, &normalBuffer);
	if (textureCoordBuffer > 0)
		glDeleteBuffers(1, &textureCoordBuffer);
	if (texImage != NULL)
		delete texImage;
	if (texID > 0)
		glDeleteTextures(1, &texID);
	glDeleteVertexArrays(1, &vao);
	if (theShape != NULL)
		delete theShape;
}

void SimpleShapeViewer::defineGeometry()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// TESTING: Uncomment exactly one of the "theShape =" assignment statements below...

	cryph::AffPoint Pbottom(-1.0, -1.0, -1.0), Ptop(1.0, 1.0, 1.0);
	// CYLINDER: defaults to "cap at both ends" and no texture
//	theShape = SimpleShape::makeBoundedCylinder(Pbottom, Ptop, 0.25, 20, 2);

	// CYLINDER: with caps; texture coordinates for one full texture
	theShape = SimpleShape::makeBoundedCylinder(Pbottom, Ptop, 0.3, 50, 50, SimpleShape::CAP_AT_BOTH, 0.0, 1.0, 0.0, 1.0);
	
	// CONE: defaults to "cap at both ends" and no texture
//	theShape = SimpleShape::makeBoundedCone(Pbottom, Ptop, 0.45, 0.15, 20, 2);

	// CONE: uses no caps, and still default to no texture:
//	theShape = SimpleShape::makeBoundedCone(Pbottom, Ptop, 0.45, 0.05, 51, 51, SimpleShape::CAP_AT_NEITHER, 0.2, 0.8, 0.0, 1.0);

	cryph::AffPoint sCenter(0.0, 0.0, 0.0);
	double radius = 1.0;

	// SPHERE: uses all defaults
//	theShape = SimpleShape::makeSphere(sCenter, radius, 50, 51);

	// SPHERE: applies texture only around middle of sphere
//	theShape = SimpleShape::makeSphere(sCenter, radius, 50, 51, 0.0, 1.0, -0.5, 1.5, cryph::AffVector::zu, cryph::AffVector::yu);

	// SPHERE: works well with glasses
//	theShape = SimpleShape::makeSphere(sCenter, radius, 50, 51, 0.0, 3.0, -3.25, 2.75, cryph::AffVector::zu, cryph::AffVector::yu);

	if (theShape == NULL)
	{
		std::cerr << "Could not create shape!\n";
		return;
	}

	int nPoints = theShape->getNumPoints();

	// There is always a vertex buffer:
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	const float* pts = theShape->getPointCoords();
	glBufferData(GL_ARRAY_BUFFER, nPoints*3*sizeof(float), pts, GL_STATIC_DRAW);
	glEnableVertexAttribArray(SimpleShapeViewer::aLoc_mcPosition);
	glVertexAttribPointer(SimpleShapeViewer::aLoc_mcPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// there is always a normal buffer
	glGenBuffers(1, &normalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	const float* normals = theShape->getNormals();
	glBufferData(GL_ARRAY_BUFFER, nPoints*3*sizeof(float), normals, GL_STATIC_DRAW);
	glEnableVertexAttribArray(SimpleShapeViewer::aLoc_mcNormal);
	glVertexAttribPointer(SimpleShapeViewer::aLoc_mcNormal, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// there may or may not be texture coordinates
	const float* texCoords = theShape->getTextureCoords();
	if (texCoords == NULL)
		glDisableVertexAttribArray(SimpleShapeViewer::aLoc_texCoords);
	else
	{
		glGenBuffers(1, &textureCoordBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, textureCoordBuffer);
		glBufferData(GL_ARRAY_BUFFER, nPoints*2*sizeof(float), texCoords, GL_STATIC_DRAW);
		glEnableVertexAttribArray(SimpleShapeViewer::aLoc_texCoords);
		glVertexAttribPointer(SimpleShapeViewer::aLoc_texCoords, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}
}

void SimpleShapeViewer::defineLightingEnvironment()
{
	const int numLights = 2;
	glUniform1i(uLoc_actualNumLights, numLights);
	float lightPos[4*numLights] =
	{
		0.25, 0.5, 1.0, 0.0, // directional
		0.0, 0.0, 0.0, 1.0   // point (at eye)
	};
	glUniform4fv(uLoc_ecLightPosition, numLights, lightPos);
	float lightStrength[4*numLights] =
	{
		0.8, 0.8, 0.8, 1.0, // 80% white
		0.8, 0.8, 0.8, 1.0  // 80% white
	};
	glUniform4fv(uLoc_lightStrength, numLights, lightStrength);
	float spotDir[3*numLights] =
	{
		0.0, 0.0, 0.0,  // these values are irrelevant since light source 0 is directional
		0.0, 0.0, -1.0  // spot light pointing along line of sight
	};
	glUniform3fv(uLoc_spotDir, numLights, spotDir);
	float spotParams[2*numLights] =
	{
		0.0, 0.0,  // these values are irrelevant since light source 0 is directional
		// uncomment one of the following two lines, depending on the desired parameters for light source 1
		10.0, 25.0 // ten degree field of view; exponent is 25
		//			180.0, 0.0 // point source, but not a spot light
	};
	glUniform2fv(uLoc_spotParams, numLights, spotParams);
	
	float globalAmbient[] = { 0.1, 0.1, 0.1, 1.0 };
	glUniform4fv(uLoc_globalAmbient, 1, globalAmbient);
}

void SimpleShaklpeViewer::defineTexture()
{
	if (texImage == NULL)
		return;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);
	float white[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, white);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	GLint level = 0;
	int pw = texImage->getWidth(), ph = texImage->getHeight();
	GLenum iFormat = texImage->getInternalFormat();
	GLenum format = texImage->getFormat();
	GLenum type = texImage->getType();
	const GLint border = 0; // must be zero (only present for backwards compatibility)
	const GLvoid* pixelData = texImage->getTexture();
	glTexImage2D(GL_TEXTURE_2D, level, iFormat, pw, ph, border, format, type, pixelData);
}

void SimpleShapeViewer::doInitialize()
{
	if (SimpleShapeViewer::shaderProgram > 0)
	{
		// record attribute locations
		SimpleShapeViewer::aLoc_mcPosition = pvAttribLocation(shaderProgram, "mcPosition");
		SimpleShapeViewer::aLoc_mcNormal = pvAttribLocation(shaderProgram, "mcNormal");
		SimpleShapeViewer::aLoc_texCoords = pvAttribLocation(shaderProgram, "texCoords");
		// record uniform locations
		SimpleShapeViewer::uLoc_ecLightPosition = ppUniformLocation(shaderProgram, "ecLightPosition");
		SimpleShapeViewer::uLoc_actualNumLights = ppUniformLocation(shaderProgram, "actualNumLights");
		SimpleShapeViewer::uLoc_lightStrength = ppUniformLocation(shaderProgram, "lightStrength");
		SimpleShapeViewer::uLoc_spotDir = ppUniformLocation(shaderProgram, "spotDir");
		SimpleShapeViewer::uLoc_spotParams = ppUniformLocation(shaderProgram, "spotParams");
		SimpleShapeViewer::uLoc_globalAmbient = ppUniformLocation(shaderProgram, "globalAmbient");
		SimpleShapeViewer::uLoc_modelViewMatrix = ppUniformLocation(shaderProgram, "modelViewMatrix");
		SimpleShapeViewer::uLoc_projectionMatrix = ppUniformLocation(shaderProgram, "projectionMatrix");
		SimpleShapeViewer::uLoc_normalMatrix = ppUniformLocation(shaderProgram, "normalMatrix");
		SimpleShapeViewer::uLoc_kakdks = ppUniformLocation(shaderProgram, "kakdks");
		SimpleShapeViewer::uLoc_shininess = ppUniformLocation(shaderProgram, "shininess");
		SimpleShapeViewer::uLoc_haveTextureMap = ppUniformLocation(shaderProgram, "haveTextureMap");
		SimpleShapeViewer::uLoc_textureMap = ppUniformLocation(shaderProgram, "textureMap");
	}
	defineGeometry();
}

void SimpleShapeViewer::drawShape(const SimpleShape* aShape)
{
	if (aShape == NULL)
		return;

	// set the shape's material properties:
	// ambient, diffuse, and specular reflectances along with specular exponent
	float kakdks[] = { 0.2, 0.0, 0.0, 1.0, 0.8, 0.0, 0.0, 1.0, 0.2, 0.0, 0.0, 1.0 };
	glUniform4fv(uLoc_kakdks, 3, kakdks);
	glUniform1f (uLoc_shininess, 20.0);
	
	GLenum mode;
	// draw the curved surface part
	// first see if a texture is to be applied
	if (texImage == NULL)
		glUniform1i(uLoc_haveTextureMap, 0);
	else
	{
		glUniform1i(uLoc_haveTextureMap, 1);
		glUniform1i(uLoc_textureMap, 0); // refers to '0' in glActiveTexture(GL_TEXTURE0);
	}
	int offset;
	int nPoints = aShape->getDrawArraysData(mode, offset);
	if (nPoints > 0)
	{
		glEnableVertexAttribArray(SimpleShapeViewer::aLoc_mcNormal);
		glDrawArrays(mode, offset, nPoints);
	}

	// SimpleShape may also have index list data to be drawn (e.g., caps, if present;
	// also index lists are used if nPointsAlongAxis > 2).
	int nInList;
	GLenum type;
	cryph::AffVector fixedN;
	float normal[3];
	bool canUseTexCoordArray, canUseNormalArray;
	for (int i=0 ; i<aShape->getNumIndexLists() ; i++)
	{
		const void* indices = aShape->getIndexList(i, mode, nInList, type,
									canUseTexCoordArray, canUseNormalArray, fixedN);
		if (canUseNormalArray)
			glEnableVertexAttribArray(SimpleShapeViewer::aLoc_mcNormal);
		else
		{
			glDisableVertexAttribArray(SimpleShapeViewer::aLoc_mcNormal);
			glVertexAttrib3fv(SimpleShapeViewer::aLoc_mcNormal, fixedN.vComponents(normal));
		}
		if (canUseTexCoordArray)
		{
			// first see if a texture is to be applied
			if (texImage == NULL)
				glUniform1i(uLoc_haveTextureMap, 0);
			else
			{
				glUniform1i(uLoc_haveTextureMap, 1);
				glBindTexture(GL_TEXTURE_2D, texID);
				glUniform1i(uLoc_textureMap, 0); // refers to texture unit '0', the only one this program uses
			}
		}
		else
			glUniform1i(uLoc_haveTextureMap, 0);
		glDrawElements(mode, nInList, type, indices);
	}
}

void SimpleShapeViewer::getWCBoundingBox(float* xyzLimits) const
{
	xyzLimits[0] = -1.5; xyzLimits[1] = 1.5;
	xyzLimits[2] = -1.5; xyzLimits[3] = 1.5;
	xyzLimits[4] = -1.5; xyzLimits[5] = 1.5;
}

void SimpleShapeViewer::render()
{
	// save the current and set the new shader program
	int savedPgm = 0;
	if (SimpleShapeViewer::shaderProgram > 0)
	{
		// save the current GLSL program in use
		glGetIntegerv(GL_CURRENT_PROGRAM, &savedPgm);
		// draw the triangles using our vertex and fragment shaders
		glUseProgram(SimpleShapeViewer::shaderProgram);
	}

	// establish the lighting environment
	defineLightingEnvironment();

	// establish global matrices
	const cryph::Matrix4x4 *modelViewMatrix, *projectionMatrix;
	const cryph::Matrix3x3 *normalMatrix;
	getMatrices(modelViewMatrix, normalMatrix, projectionMatrix);
	float m[16];
	glUniformMatrix4fv(uLoc_modelViewMatrix, 1, false, modelViewMatrix->extractColMajor(m));
	glUniformMatrix4fv(uLoc_projectionMatrix, 1, false, projectionMatrix->extractColMajor(m));
	glUniformMatrix3fv(uLoc_normalMatrix, 1, false, normalMatrix->extractColMajor(m));

	// for each shape, do:
	// (Here we only have one...)
	drawShape(theShape);

	// restore the saved program
	if (savedPgm > 0)
		// restore the previous program, if any (i.e., if savedPgm > 0)
		glUseProgram(savedPgm);
}

void SimpleShapeViewer::setTextureImage(const char* imgFileName)
{
	if (imgFileName == NULL)
		return;
	texImage = ImageReader::create(imgFileName);
	defineTexture();
}
