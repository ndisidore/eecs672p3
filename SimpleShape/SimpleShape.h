// SimpleShape.h - Interface for common shapes in OpenGL

#ifndef SIMPLESHAPE_H
#define SIMPLESHAPE_H

#include <GL/gl.h>
#include "../PointVectorTools/AffPoint.h"
#include "../PointVectorTools/AffVector.h"

class SimpleShape
{
public:
	virtual ~SimpleShape();

	enum Caps { CAP_AT_NEITHER, CAP_AT_BOTTOM, CAP_AT_TOP, CAP_AT_BOTH };

	// Factory methods for creation of shapes:
	// Texture coordinates are generated only if BOTH sMax>sMin AND tMax>tMin;
	// 's' direction is around the circumference of the cylinder/cone; 't' direction
	// is along the axis. "nPointsAroundSide" must be at least 3. "nPointsAlongAxis"
	// must be at least 2. If invalid input is given (e.g., negative radii, coincident
	// points, bad "nPoints*" specs, etc.), NULL is returned.
	// The component of the optional vector "sZero" that is perpendicular to the "axis"
	// will define the direction for texture s coordinate=0. It serves no other
	// externally significant purpose.
	static SimpleShape* makeBoundedCone(
		const cryph::AffPoint& Pbottom, const cryph::AffPoint& Ptop,
		double radiusAtBottom, double radiusAtTop,
		int nPointsAroundSide, int nPointsAlongAxis,
		Caps capSpec=CAP_AT_BOTH,
		double sMin=0, double sMax=0, double tMin=0, double tMax=0,
		const cryph::AffVector& sZero=cryph::AffVector::xu);
	
	static SimpleShape* makeBoundedCylinder(
		const cryph::AffPoint& Pbottom, const cryph::AffPoint& Ptop,
		double radius,
		int nPointsAroundSide, int nPointsAlongAxis,
		Caps capSpec=CAP_AT_BOTH,
		double sMin=0, double sMax=0, double tMin=0, double tMax=0,
		const cryph::AffVector& sZero=cryph::AffVector::xu);

	// 's' and 't' directions are analogous to those for cylinder. (See parameter "upAxis".)
	// nPointsAlongAxis must be at least 3. In general, an odd number will work best because
	// that forces points along the equator to be generated.
	static SimpleShape* makeSphere(
		const cryph::AffPoint& center, double radius,
		int nPointsAroundSide, int nPointsAlongAxis,
		double sMin=0, double sMax=0, double tMin=0, double tMax=0,
		const cryph::AffVector& upAxis=cryph::AffVector::yu,
		const cryph::AffVector& sZero=cryph::AffVector::xu);
	
	// Accessors for use during transmission of definition to GPU (e.g., glBufferData, et al.)
	// 1. number of points, normals, and (if applicable) texture coords:
	int getNumPoints() const { return nPoints; }
	// 2. The actual point, normal, and texture coordinates:
	const float* getPointCoords() const { return pointCoords; }
	const float* getNormals() const { return normals; }
	const float* getTextureCoords() const { return textureCoords; }

	// Accessors for use during actual drawing during display callbacks
	// May have data for a glDrawArrays call:
	int getDrawArraysData(GLenum& mode, int& offset) const; // func return is 0 or # points
	// May also (or instead) have one or more sets of data for glDrawElements calls:
	int getNumIndexLists() const { return nIndexLists; }
	const void* getIndexList(int i, GLenum& mode, int& nInList, GLenum& type,
			bool& canUsePerVertexTexCoords, bool& canUsePerVertexNormals,
			cryph::AffVector& fixedNormal) const;

private:
	SimpleShape(); // use public factory methods instead
	SimpleShape(const SimpleShape& s) {} // don't make copies

	// common "worker" routines for cones and cylinders:
	void addCaps(Caps capSpec, const cryph::AffVector& axis, int nPointsAlongAxis);
	void allocateIndexListsForConeCyl(int nPointsAlongAxis, Caps capSpec);
	void allocateIndexListsForSphere(int nPointsAlongAxis);
	void makeIndexLists(int nPointsAroundSide, int nPointsAlongAxis);
	void makeRuledSurfaceBetweenCircles(
		const cryph::AffPoint& Pbottom, cryph::AffVector& axis,
		double height, double radiusAtBottom, double radiusAtTop,
		int nPointsAroundSide, int nPointsAlongAxis,
		Caps capSpec, double sMin, double sMax, double tMin, double tMax,
		const cryph::AffVector& sZero);
	void makeSphere(const cryph::AffPoint& center,
					const cryph::AffVector& u, const cryph::AffVector& v, const cryph::AffVector& w,
					double radius,
					int nPointsAroundSide, int nPointsAlongAxis,
					double sMin, double sMax, double tMin, double tMax);

	GLenum drawArraysMode;
	bool hasDrawArraysData;
	float* pointCoords;
	float* normals;
	float* textureCoords;
	int nPoints;

	struct IndexListData
	{
		GLenum mode;
		int* indices;
		int nIndices;
		bool usePerVertexTexCoords, usePerVertexNormals;
		cryph::AffVector useFixedNormal; // if !usePerVertexNormals
	};
	IndexListData* indexLists;
	int nIndexLists, nIndexListsConstructed;
};

#endif
