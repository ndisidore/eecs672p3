// main.c++

#include <GL/freeglut.h>

#include "Controller.h"
#include "SimpleShapeViewer.h"

int main(int argc, char* argv[])
{
	// One-time initialization of the glut
	glutInit(&argc, argv);

	Controller c(argv[0]);
	SimpleShapeViewer* ssv = new SimpleShapeViewer();
	if (argc > 1)
		ssv->setTextureImage(argv[1]);
	c.addModel( ssv );

	// Off to the glut event handling loop:
	glutMainLoop();

	return 0;
}
