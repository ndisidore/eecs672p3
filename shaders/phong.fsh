#version 330

// simpleColorPlusTexture.fsh

uniform int haveTextureMap;
uniform sampler2D textureMap;

// Input from vertex shader programs:
in vec4 interpolatedColor;
in vec2 texCoordsToFS; // (s,t)

// and output:
out vec4 fragColor;

void main (void) 
{
	if (haveTextureMap == 1)
		fragColor = interpolatedColor * texture(textureMap, texCoordsToFS);
	else
		fragColor = interpolatedColor;
	gl_FragDepth = gl_FragCoord.z;
}
