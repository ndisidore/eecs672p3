#version 330

// phong.vsh - simple Phong Model; assumes incoming geometry
//             in affine (xyz) space. Needs minor adjustments if,
//             for example, used with rational curves where (xyzw)
//             would come in.

// Per-primitive attributes
// 1. Light sources
const int MAX_NUM_LIGHTS = 3;
uniform int actualNumLights;
uniform vec4 ecLightPosition[MAX_NUM_LIGHTS]; // position assumed to be in eye coordinates
// positional sources can be spotlights:
uniform vec3 spotDir[MAX_NUM_LIGHTS]; // only used if this really is a spot light
uniform vec2 spotParams[MAX_NUM_LIGHTS]; // [i][0]: cutoffAngleInDegrees (spotlight if and only if < 90); [i][1]: exponent
// end spotlight data
uniform vec4 lightStrength[MAX_NUM_LIGHTS]; // (r,g,b,a) strength
uniform vec4 globalAmbient;
// 2. Transformation
uniform mat4 modelViewMatrix, projectionMatrix;
uniform mat3 normalMatrix;
// 3. Material properties
uniform vec4 kakdks[3];
uniform float shininess;

// Per-vertex attributes
in vec3 mcPosition; // position in modeling coordinates
in vec3 mcNormal; // normal vector in modeling coordinates
in vec2 texCoords; // (s,t)

// LOCAL ACCUMULATION VARIABLES
vec4 Diffuse;
vec4 Specular;

// Output to fragment shader:
out vec4 interpolatedColor;
out vec2 texCoordsToFS; // (s,t)

void pointLight(in int i, in vec3 nHat, in vec3 P)
{
	// Compute vector from surface to light position
	vec3 li = ecLightPosition[i].xyz - P;
	
	// Compute distance between surface and light position (in case we add attenuation later)
	float d = length(li);
	
	// Normalize the vector from surface to light position
	vec3 liHat = li/d;
	
	// compute effective light strength. This is just the regular light strength
	// if the source is NOT a spotlight; otherwise it is attenuated, and perhaps cut to zero:
	vec4 effectiveLightStrength = lightStrength[i];
	if ((spotParams[i][0] > 0.0) && (spotParams[i][0] < 90.0))
	{
		// yes, it's a spotlight
		float cutoff = cos(radians(spotParams[i][0]));
		float testDot = dot(-liHat,normalize(spotDir[i])); // -liHat so that we use light to surface direction to test if in cone
		if (testDot < cutoff)
			// point outside spot light range - can't contribute at all
			return;
		effectiveLightStrength *= pow(testDot, spotParams[i][1]);
	}
	
	// "nHat" is assumed to have unit length on entry; P is in eye coordinates
	// vector to the eye is E-P=(0,0,0)-P, hence
	vec3 vHat = -normalize(P);
	vec3 use_nHat = nHat;
	if (dot(vHat,nHat) < 0.0)
		// we are looking at this surface from behind; flip the normal
		use_nHat = -use_nHat;

	// diffuse term:
	float nHatDotLiHat = dot(use_nHat, liHat);

	if (nHatDotLiHat > 0.0)
	{
		Diffuse  += effectiveLightStrength * nHatDotLiHat;
		vec3 riHat = 2.0*nHatDotLiHat*use_nHat - liHat;
		float riHatDotVHat = dot(riHat,vHat);
		if (riHatDotVHat > 0.0)
			Specular += effectiveLightStrength * pow(riHatDotVHat, shininess);
	}
}

void directionalLight(in int i, in vec3 nHat, in vec3 P)
{
	// "nHat" is assumed to have unit length on entry; P is in eye coordinates
	// vector to the eye is E-P=(0,0,0)-P, hence
	vec3 vHat = -normalize(P);
	vec3 use_nHat = nHat;
	if (dot(vHat,nHat) < 0.0)
		// we are looking at this surface from behind; flip the normal
		use_nHat = -use_nHat;

	vec3 liHat = normalize(ecLightPosition[i].xyz);
	float nHatDotLiHat = dot(use_nHat, liHat);

	if (nHatDotLiHat > 0.0)
	{
		Diffuse  += lightStrength[i] * nHatDotLiHat;
		vec3 riHat = 2.0*nHatDotLiHat*use_nHat - liHat;
		float riHatDotVHat = dot(riHat,vHat);
		if (riHatDotVHat > 0.0)
			Specular += lightStrength[i] * pow(riHatDotVHat, shininess);
	}
}

void phong(in vec3 ecPos, in vec3 ec_nHat)
{
	// "nHat" has unit length on entry

	// Clear the light intensity accumulators
	Diffuse  = vec4 (0.0);
	Specular = vec4 (0.0);

	for (int lsi=0 ; lsi<actualNumLights ; lsi++)
	{
		if (ecLightPosition[lsi].w > 0.0)
			pointLight(lsi, ec_nHat, ecPos);
		else
			directionalLight(lsi, ec_nHat, ecPos);
	}

	vec4 color = globalAmbient * kakdks[0] + Diffuse  * kakdks[1] + Specular * kakdks[2];
	interpolatedColor = clamp( color, 0.0, 1.0 );
}


void main (void)
{
	// convert current vertex and its associated normal to eye coordinates
	// ("p_" prefix emphasizes it is stored in projective space)
	vec4 p_ecPosition = modelViewMatrix * vec4(mcPosition, 1.0);
	vec3 ec_nHat = normalize(normalMatrix * mcNormal);
	// compute vertex color from Phong model
	phong(p_ecPosition.xyz, ec_nHat); // we implicitly assume p_ecPosition.w=1
	
	texCoordsToFS = texCoords;

	// need to compute projection coordinates for given point
	gl_Position = projectionMatrix * p_ecPosition;
}
