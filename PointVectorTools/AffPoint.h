// AffPoint.h -- 3D Affine points

/*
Copyright (c) 1998-2012, cryph Component Library and Tools
James R. Miller, University of Kansas, Lawrence, Kansas, U.S.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

   * Neither the name of the cryph Component Library and Tools
     project nor the names of its contributors may be used to endorse
     or promote products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS OPEN SOURCE. IT IS NOT IN THE PUBLIC DOMAIN.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

// When spherical coordinates are used below, the meaning of the (theta,phi)
// angles is the following. Assume R is the vector from the origin to a
// given point in space. Assume Rxy is the projection of the vector R onto the
// xy-plane. Then:
//     phi   = angle between R and the z-axis (0 <= phi <= PI)
//     theta = angle from the x-axis to Rxy. Positive angles are counter-
//             clockwise from the x-axis. Normal range: (-PI <= theta <= +PI).
// Notice that this convention is different from the convention used in some
// contexts such as image synthesis where the roles of theta and phi are often
// reversed, probably because theta has historically been used in Image
// Synthesis to characterize an angle of incidence. That is, to describe the
// angle between the outward pointing normal vector to a surface (a local "z"
// axis) and an incoming light direction.

#ifndef AFFPOINT_H
#define AFFPOINT_H
namespace cryph { class AffPoint; }
#include "Basic.h"
#include "IOSpec.h"

#include "AffVector.h"

namespace cryph
{

// indices for extracting coordinates from points

const int W = -1;
const int X =  0;
const int Y =  1;
const int Z =  2;

class AffPoint
{
public:

	// --------- basic constructors
	AffPoint(); // point at origin
	AffPoint(double xx, double yy, double zz=0.0);
	AffPoint(const AffPoint& p);
	AffPoint(const double* p); // assumes x=p[0], y=p[1], z=p[2]
	AffPoint(const float* p); // assumes x=p[0], y=p[1], z=p[2]

	// --------- other constructors
	AffPoint(const AffVector& v);

	// --------- destructor
	virtual ~AffPoint();

	// --------- operators
	AffPoint	operator=(const AffPoint& rhs);
	AffPoint	operator+=(const AffVector& rhs);
	AffPoint	operator+=(const AffPoint& rhs);
	AffPoint	operator-=(const AffVector& rhs);
	AffPoint	operator*=(double f);
	AffPoint	operator/=(double f);
	double		operator[](int index) const; // read-only indexing
					// see indexing constants above

	AffPoint	operator+(const AffPoint& p2) const
				{ return AffPoint(x + p2.x, y + p2.y, z + p2.z); }
	AffPoint	operator*(double f) const
				{ return AffPoint (f*x, f*y, f*z); }
	AffPoint	operator/(double f) const
				{ return AffPoint (x/f, y/f, z/f); }
	AffVector	operator-(const AffPoint& p2) const
				{ return AffVector(x-p2.x, y-p2.y, z-p2.z); }
	AffPoint	operator+(const AffVector& v2) const
				{ return AffPoint(x+v2[DX], y+v2[DY], z+v2[DZ]); }
	AffPoint	operator-(const AffVector& v2) const
				{ return AffPoint(x-v2[DX], y-v2[DY], z-v2[DZ]); }

	// ---------- General Methods
	//                  Extract affine coordinates into ...
	double*				aCoords(double* coords, int offset=0) const;
	float*				aCoords(float* coords, int offset=0) const;
	void				barycentricCoords( // in a plane
							// find the areal Barycentric coordinates of "this"
							// point with respect to:
							const AffPoint& P1, const AffPoint& P2,
							const AffPoint& P3,
							// returning them in:  (b1+b2+b3 = 1)
							double& b1, double& b2, double& b3) const;
	void				barycentricCoords( // on a line
							// find the areal Barycentric coordinates of "this"
							// point with respect to:
							const AffPoint& P1, const AffPoint& P2,
							// returning them in:  (b1+b2 = 1)
							double& b1, double& b2) const;
	bool				coincidentWith(const AffPoint& p) const;
	double				distanceFromLine(const AffPoint& B, const AffVector& u)
							const;
	double				distanceFromOrigin() const;
	double				distanceSquaredFromLine(
							const AffPoint& B, const AffVector& u) const;
	double				distanceSquaredFromOrigin() const;
	double				distanceSquaredTo(const AffPoint& p) const;
	double				distanceTo(const AffPoint& p) const;
	double*				firstCoordP() { return &x; }
	double				normalize(); // move point along line through
							// origin until it lies on the unit sphere
	double*				pCoords(double* coords, double w) const;
	double*				pCoords(double* coords, double w, int offset) const;
								// extract coords, represent in projective
								// space using provided "w" -> double array
	float*				pCoords(float* coords, double w) const;
	float*				pCoords(float* coords, double w, int offset) const;
								// extract coords, represent in projective
								// space using provided "w" -> single array
	void				toCylindrical(double& r, double& theta, double& z) const;
	void				toSpherical(double& rho, double& theta, double& phi) const;

	// ---------- General Class Methods
	static AffPoint		centroid(const AffPoint p[], int nPoints);
	static AffPoint		fromBarycentricCoords( // in a plane
							const AffPoint& P1, const AffPoint& P2,
							const AffPoint& P3,
							double b1, double b2, double b3)
						{ return b1*P1 + b2*P2 + b3*P3; }
	static AffPoint		fromBarycentricCoords( // on a line
							const AffPoint& P1, const AffPoint& P2,
							double b1, double b2)
						{ return b1*P1 + b2*P2; }
	static AffPoint		fromCylindrical(double r, double theta, double z)
						{ return AffPoint( r*cos(theta), r*sin(theta), z ); }
	static AffPoint		fromSpherical(double rho, double theta, double phi)
						{ return AffPoint(rho*sin(phi)*cos(theta),
										  rho*sin(phi)*sin(theta), rho*cos(phi) ); }
	static double		getCoincidenceTolerance() { return AffPoint::sCoincidenceTol; }
	static char*		getInputFormat(int iAspect);
	static IOSpec*		getInputSpec() { return &iSpec; }
	static char*		getOutputFormat(int oAspect);
	static IOSpec*		getOutputSpec() { return &oSpec; }
	static void			inputFormat(int iAspect, char iValue);
	static void			inputFormat(int iAspect, const char* iValue);
	static void			ioFormat(int ioAspect, char ioValue);
	static void			ioFormat(int ioAspect, const char* ioValue);
					// find the point in the given buffer which is farthest
					// away in the given direction. If that point is 'P', the
					// function return value is "(P-ref).dir". If there is
					// exactly one such farthest point, then the indices i1
					// and i2 will both contain the index of 'P' in 'buf'. If
					// several points tie, then i1 and i2 are the indices of the
					// first succession of adjacent points all at that offset.
					// If there are multiple sets of adjacent tieing points,
					// only the indices of the first are returned.
	static double			maxOffsetInDirection(
							const AffPoint& ref, const AffVector& dir,
							const AffPoint buf[], int bufSize,
							int& index1, int& index2);
	static void			outputFormat(int oAspect, char oValue);
	static void			outputFormat(int oAspect, const char* oValue);
	static double			ratio(const AffPoint& a, const AffPoint& b,
							const AffPoint& c);
	static void			setCoincidenceTolerance(double tol);

	// caller responsible to do delete[] in "toCoordArray"
	static double*	toCoordArray(AffPoint* pts, int nPts);
	// in following, a private (to AffPoint) array is used and the caller
	// cannot rely on the data staying untouched for long. (Next call to
	// this method will overwrite and possible relocate the array.) This
	// routine is useful for feeding OpenGL routines like glVertexPointer.
	static double*	toTransientCoordArray(AffPoint* pts, int nPts);
	static double* transientBuffer;
	static int transientBufferLength;
	
    // ---------- Global constants

	// 1. Special Points
	static const AffPoint		origin;
	static const AffPoint		xAxisPoint;
	static const AffPoint		yAxisPoint;
	static const AffPoint		zAxisPoint;

	// 2. Values for "ioAspect" in "ioFormat"
	static const int		openDelimiter;		// e.g.: '(', '[', '{', '<', ' '
	static const int		separator;			// e.g.: ' ', '\t', '\n', ','
	static const int		closeDelimiter;

private:

	// --------- instance variables
	double	x;
	double	y;
	double	z;

	static double	sCoincidenceTol;

	static IOSpec	iSpec; // for input
	static IOSpec	oSpec; // for output

	// --------- private methods
};
	std::ostream&		operator<<(std::ostream& os, const AffPoint& p);
	std::istream&		operator>>(std::istream& is, AffPoint& p);
	
	static AffPoint operator*(double f, const AffPoint& p)
	{ return AffPoint(f*p[X], f*p[Y], f*p[Z]); }
}

#endif
