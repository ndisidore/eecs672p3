// Basic.h  -- 1. Basic numeric type and constant declarations
//             2. Template based dynamic allocation for multidimensional array
//                memory management
//             3. Non-template based dynamic allocation for multidimensional
//                array memory management
//             4. Some basic I/O and character-based processing, including
//                implementations of standard functions not available in all
//                compiler environments

/*
Copyright (c) 1998-2012, cryph Component Library and Tools
James R. Miller, University of Kansas, Lawrence, Kansas, U.S.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

   * Neither the name of the cryph Component Library and Tools
     project nor the names of its contributors may be used to endorse
     or promote products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS OPEN SOURCE. IT IS NOT IN THE PUBLIC DOMAIN.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BASIC_H
#define BASIC_H

#include <iostream>
#include <fstream>
#include <string>

#include <math.h>
#include <stdlib.h>

namespace cryph { class AffPoint; class AffVector; }

// some common trig constants

#ifdef PI
#undef PI
#endif

#ifdef PI_OVER_2
#undef PI_OVER_2
#endif

#ifdef TWO_PI
#undef TWO_PI
#endif

/*  If the compiler does not support these:

#include <string.h>

static char* strdup(const char* s)
{
	char* dup = new char[strlen(s)+1];
	strcpy(dup,s);
	return dup;
}
#include <ctype.h>
static int strcasecmp(const char* s1, const char* s2)
{
	char* locs1 = strdup(s1);
	char* locs2 = strdup(s2);

	char* s = locs1;
	while (*s != '\0')
	{
		*s = tolower(*s);
		s++;
	}
	s = locs2;
	while (*s != '\0')
	{
		*s = tolower(*s);
		s++;
	}
	int answer = strcmp(locs1,locs2);
	delete [] locs1;
	delete [] locs2;
	return answer;
}

*********/
 
namespace cryph
{
typedef double real; // for compatibility with old code...

// some basic tolerances
extern double	BasicAngleTol;
extern double	BasicDistanceTol;
extern double	BasicDistanceSquaredTol;
extern double	BasicUnitTol;

#ifdef M_PI
const double	PI				= M_PI;
const double	PI_OVER_2		= M_PI_2;
#else
const double	PI				= atan2(0.0,-1.0);
const double	PI_OVER_2		= 0.5 * PI;
#endif
const double	THREE_PI_OVER_2	= PI + PI_OVER_2;
const double	TWO_PI			= PI + PI;

// Function prototypes

// 1. Dynamic Allocation
// Non-template versions of 2D/3D array allocation/deallocation do not
// require an initial value to be specified.

void		delete2DArrayD(double**& a);
void		delete2DArrayF(float**& a);
void		delete3DArrayD(double***& a);
void		delete3DArrayF(float***& a);
double**	new2DArrayD(int rows, int cols);
float**		new2DArrayF(int rows, int cols);
double***	new3DArrayD(int dim1, int dim2, int dim3);
float***	new3DArrayF(int dim1, int dim2, int dim3);
void		outputElement(std::ostream& os, double elem);

// 2. Other basic stuff

char*		copyString(const char* str); // returns a 'new'-ly allocated copy
int			intLinePlane(
				const AffPoint& linePnt,  const AffVector& lineDir,
				const AffPoint& plnPoint, const AffVector& plnNormal,
				AffPoint& intPoint);
std::string lowerCaseString(const std::string& str);
bool		pointOnPlane(AffPoint Q, AffPoint plnPoint, AffVector plnNormal);

// compute (a,b) so that a value, v, in the "from" range is
// mapped to a corresponding value in the "to" range as:
//   vTo = a*vFrom + b
void linearMap(double fromMin, double fromMax, double toMin, double toMax,
                        double& a, double& b);

// 3. Template versions of Dynamic allocation

// Template versions of 2D/3D array allocation/deallocation. An initial
// value must be specified, partly because C++ cannot recognize the correct
// template reference unless an item based on type "T" appears in the formal
// parameter list.

template <typename T>
void delete2DArray(T**& a)
{
	if (a == NULL)
		return;

	int i = 0;
	while (a[i] != NULL)
	{
		delete [] a[i];
		a[i++] = NULL;
	}
	delete [] a;
	a = NULL;
}

template <typename T>
void delete3DArray(T***& a)
{
	if (a == NULL)
		return;

	int i = 0;
	while (a[i] != NULL)
	{
		int j = 0;
		while (a[i][j] != NULL)
		{
			delete [] a[i][j];
			a[i][j++] = NULL;
		}
		delete [] a[i];
		a[i++] = NULL;
	}
	delete [] a;
	a = NULL;
}

template <typename T>
T**			new2DArray(int nRows, int nCols, const T& initialValue)
{
	T** a = new T*[nRows+1]; // one extra to use as a "sentinel" for "delete"
	for (int i=0 ; i<nRows ; i++)
	{
		a[i] = new T[nCols];
		for (int j=0 ; j<nCols ; j++)
			a[i][j] = initialValue;
	}
	a[nRows] = NULL;
	return a;
}

template <typename T>
T*** new3DArray(int dim1, int dim2, int dim3, const T& initialValue)
{
	T*** a = new T**[dim1+1]; // add a sentinel for "delete" to find
	for (int i=0 ; i<dim1 ; i++)
	{
		a[i] = new T*[dim2+1]; // add a sentinel for "delete" to find
		for (int j=0 ; j<dim2 ; j++)
		{
			a[i][j] = new T[dim3];
			for (int k=0 ; k<dim3 ; k++)
				a[i][j][k] = initialValue;
		}
		a[i][dim2] = NULL;
	}
	a[dim1] = NULL;
	return a;
}

} // end namespace cryph
 
#endif
