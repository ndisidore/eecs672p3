/* Inline.h  -- Definition of common low-level inline functions */
//  Written by James R. Miller (jrmiller@ku.edu)
//  Department of Electrical Engineering and Computer Science
//  The University of Kansas

/*
Copyright (c) 1998-2012 cryph Component Library and Tools
James R. Miller, University of Kansas, Lawrence, Kansas, U.S.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

   * Neither the name of the cryph Component Library and Tools
     project nor the names of its contributors may be used to endorse
     or promote products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS OPEN SOURCE. IT IS NOT IN THE PUBLIC DOMAIN.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef INLINE_H
#define INLINE_H

#include "Basic.h"

namespace cryph
{

inline double degreesToRadians(double angleInDegrees)
{
	return angleInDegrees*PI/180.0;
}

inline bool equalScalars(double s1, double s2, double tol)
{
	return (fabs(s1-s2) < tol);
}

inline int maximum(int s1, int s2)
{
	return (s1 >= s2) ? s1 : s2;
}

inline double maximum(double s1, double s2)
{
	return (s1 >= s2) ? s1 : s2;
}

inline int minimum(int s1, int s2)
{
	return (s1 >= s2) ? s2 : s1;
}

inline double minimum(double s1, double s2)
{
	return (s1 >= s2) ? s2 : s1;
}

inline double radiansToDegrees(double angleInRadians)
{
	return angleInRadians*180.0/PI;
}

inline double random0To1_next()
{
#ifdef __MWERKS__
	return static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
#else
	return drand48();
#endif
}

inline void random0To1_seed(long seedVal)
{
#ifdef __MWERKS__
	srand(static_cast<unsigned int>(seedVal));
#else
	srand48(seedVal);
#endif
}

inline int roundR2I(double val)
{
	return (int) (val + 0.5);
}

inline void skipNonblankChars(std::istream& is, int nNonBlankChars)
{
	char ch;
	for (int i=0 ; i<nNonBlankChars ; i++)
		is >> ch;
}

inline double square(double s)
{
	return s * s;
}

// Templates

template <typename T> inline void swap2(T& t1, T& t2)
{
	T temp = t1;
	t1 = t2;
	t2 = temp;
}

}
 
#endif
