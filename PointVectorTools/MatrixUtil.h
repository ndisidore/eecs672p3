// MatrixUtil.h - A few very basic matrix utilities

#include <iostream>

extern bool inverse(float M[3][3], float mInverse[3][3]);

extern void multiply(float A[4][4], float B[4][4], float AB[4][4]);
extern void multiply(float A[3][3], float B[3][3], float AB[3][3]);

extern void showMatrix(std::ostream& os, float M[3][3]);
extern void showMatrix(std::ostream& os, float M[4][4]);

// transpose the given matrix
extern void transpose(float M[3][3]);
// transpose Matrix M into matrix MT:
extern void transpose(float M[3][3], float MT[3][3]);

extern void upper3x3(float M4x4[4][4], float U3x3[3][3]);
