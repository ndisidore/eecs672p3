// AffVector.h -- 3D vectors associated with a 3D affine space

/*
Copyright (c) 1998-2012, cryph Component Library and Tools
James R. Miller, University of Kansas, Lawrence, Kansas, U.S.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

   * Neither the name of the cryph Component Library and Tools
     project nor the names of its contributors may be used to endorse
     or promote products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS OPEN SOURCE. IT IS NOT IN THE PUBLIC DOMAIN.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef AFFVECTOR_H
#define AFFVECTOR_H

#include "Basic.h"
#include "IOSpec.h"

#ifndef AFFPOINT_H
namespace cryph { class AffPoint; }
#endif

namespace cryph
{

// indices for extracting components from vectors

const int DW = -1;
const int DX =  0;
const int DY =  1;
const int DZ =  2;

class AffVector
{
public:

	// --------- basic constructors
	AffVector(); // zero vector
	AffVector(double Dx, double Dy, double Dz=0.0);
	AffVector(const AffVector& v);

	// --------- other constructors
	AffVector(const AffPoint& p);
	AffVector(const double xyz[]); // assumes xyz contains 3D components
	AffVector(const float xyz[]);  // assumes xyz contains 3D components

	// --------- destructor
	virtual ~AffVector();

	// --------- overloaded operators

	AffVector	operator=(const AffVector& rhs);
	AffVector	operator+=(const AffVector& rhs);
	AffVector	operator-=(const AffVector& rhs);
	AffVector	operator*=(double f);
	AffVector	operator/=(double f);
	bool	operator==(const AffVector& rhs);
	bool	operator!=(const AffVector& rhs);
	double		operator[](int index) const; // read-only indexing
					// see indexing constants above

	AffVector	operator+(const AffVector& v2) const
				{ return AffVector(dx+v2.dx , dy+v2.dy , dz+v2.dz); }
	AffVector	operator-(const AffVector& v2) const
				{ return AffVector(dx-v2.dx , dy-v2.dy , dz-v2.dz); }
	AffVector	operator-() const { return AffVector(-dx, -dy, -dz); }
	AffVector	operator*(double f) const
				{ return AffVector(f*dx , f*dy , f*dz); }
	AffVector	operator/(double f) const
				{ return AffVector(dx/f , dy/f , dz/f); }

	// ---------- General Methods
	void		arbitraryNormal(AffVector& normal) const;
	AffVector	cross(const AffVector& rhs) const;
	void		decompose(const AffVector& arbitraryVector,
					AffVector& parallel, AffVector& perpendicular) const;
	double		dot(const AffVector& rhs) const
				{ return dx*rhs.dx + dy*rhs.dy + dz*rhs.dz; }
	double		length() const { return sqrt(lengthSquared()); }
	double		lengthSquared() const { return dx*dx + dy*dy + dz*dz; }
	double		maxAbsComponent(int& componentIndex) const;
	double		minAbsComponent(int& componentIndex) const;
	double		normalize();
	double		normalizeToCopy(AffVector& normalizedCopy) const;
	bool		parallelTo(const AffVector& v) const;
	void		vComponents(double* components, int offset=0) const;
	void		vComponents(float* components, int offset=0) const;

	// ---------- Class Methods
						// The following assumes U and W have values (even if
						// all zero). W gets normalized; the component of U
						// perpendicular to W becomes U. Finally V <- W x U.
						// If W is zero vector, then (xu, yu, zu) are copied
						// into U, V, and W. If the component of U perpendicular
						// to W is zero, then an arbitrary vector perpendicular
						// to W is created and used.)
	static void			coordinateSystemFromUW
							(AffVector& U, AffVector& V, AffVector& W);
						// The following is analogous to previous, but with the
						// roles of V and U reversed.
	static void			coordinateSystemFromVW
							(AffVector& U, AffVector& V, AffVector& W);
	static AffVector	cross(const AffVector& v1, const AffVector& v2);
	static double		dot(const AffVector& v1, const AffVector& v2)
						{ return v1.dx*v2.dx + v1.dy*v2.dy + v1.dz*v2.dz; }
	static char*		getInputFormat(int iAspect);
	static IOSpec*		getInputSpec() { return &iSpec; }
	static char*		getOutputFormat(int oAspect);
	static IOSpec*		getOutputSpec() { return &oSpec; }
	static void			inputFormat(int iAspect, char iValue);
	static void			inputFormat(int iAspect, const char* iValue);
	static void			ioFormat(int ioAspect, char ioValue);
	static void			ioFormat(int ioAspect, const char* ioValue);
	static void			outputFormat(int oAspect, char oValue);
	static void			outputFormat(int oAspect, const char* oValue);

	// caller responsible to do delete[] in "toCompArray"
	static double*	toCompArray(AffVector* vecs, int nVecs);
	// in following, a private (to AffVector) array is used and the caller
	// cannot rely on the data staying untouched for long. (Next call to
	// this method will overwrite and possible relocate the array.) This
	// routine is useful for feeding OpenGL routines like glNormalPointer.
	static double*	toTransientCompArray(AffVector* vecs, int nVecs);
	static double* transientBuffer;
	static int transientBufferLength;	

	// ---------- Global constants

	// 1. Special Vectors
	static const AffVector		xu;
	static const AffVector		yu;
	static const AffVector		zu;
	static const AffVector		zeroVector;

	// 2. Values for "ioAspect" in "ioFormat"
	static const int		openDelimiter;		// '(', '[', '{', or ' '
	static const int		separator;			// ' ', '\t', '\n', or ','

private:

	// --------- instance variables
	double	dx;
	double	dy;
	double	dz;

	static IOSpec	iSpec; // for input
	static IOSpec	oSpec; // for output

	// --------- private methods
};
	std::ostream&	operator<<(std::ostream& os, const AffVector& v);
	std::istream&	operator>>(std::istream& is, AffVector& v);
	static AffVector operator*(double f, const AffVector& v)
	{ return AffVector(f*v[DX] , f*v[DY] , f*v[DZ]); }
}

#endif
