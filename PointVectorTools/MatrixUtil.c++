// MatrixUtil.c++ -- A few very basic matrix utilities

#include <math.h>
#include "MatrixUtil.h"

static void subMatrix(float M[3][3], int skipRow, int skipCol, float M2x2[2][2]);

static float determinant(float m[3][3])
{
	float detPart1 = m[0][0]*(m[1][1]*m[2][2] - m[2][1]*m[1][2]);
	float detPart2 = m[0][1]*(m[1][0]*m[2][2] - m[2][0]*m[1][2]);
	float detPart3 = m[0][2]*(m[1][0]*m[2][1] - m[2][0]*m[1][1]);
	return detPart1 - detPart2 + detPart3;
}

static float determinant(float m[2][2])
{
	return m[0][0]*m[1][1] - m[1][0]*m[0][1];
}

bool inverse(float M[3][3], float mInverse[3][3])
{
	float detA = determinant(M);
	if (fabsf(detA) < 1.0e-6)
		return false;

	float subM[2][2];
	float startRowSign = 1.0;
	for (int i=0 ; i<3 ; i++)
	{
		float sign = startRowSign;
		startRowSign = -startRowSign;
		for (int j=0 ; j<3 ; j++)
		{
			subMatrix(M, j,i, subM); // note index order here and in next line
			mInverse[i][j] = sign * determinant(subM) / detA;
			sign = -sign;
		}
	}
	return true;
}

void multiply(float A[4][4], float B[4][4], float AB[4][4])
{
	for (int r=0 ; r<4 ; r++)
	{
		for (int c=0 ; c<4 ; c++)
		{
			float sum = 0.0;
			for (int k=0 ; k<4 ; k++)
				sum += A[r][k]*B[k][c];
			AB[r][c] = sum;
		}
	}
}

void multiply(float A[3][3], float B[3][3], float AB[3][3])
{
	for (int r=0 ; r<3 ; r++)
	{
		for (int c=0 ; c<3 ; c++)
		{
			float sum = 0.0;
			for (int k=0 ; k<3 ; k++)
				sum += A[r][k]*B[k][c];
			AB[r][c] = sum;
		}
	}
}

void showMatrix(std::ostream& os, float M[3][3])
{
	for (int r=0 ; r<3 ; r++)
	{
		for (int c=0 ; c<3 ; c++)
			os << M[r][c] << ' ';
		os << '\n';
	}
}

void showMatrix(std::ostream& os, float M[4][4])
{
	for (int r=0 ; r<4 ; r++)
	{
		for (int c=0 ; c<4 ; c++)
			os << M[r][c] << ' ';
		os << '\n';
	}
}

static void subMatrix(float M[3][3], int skipRow, int skipCol, float M2x2[2][2])
{
	int R = 0;
	for (int row=0 ; row<3 ; row++)
		if (row != skipRow)
		{
			int C = 0;
			for (int col=0 ; col<3 ; col++)
				if (col != skipCol)
				{
					M2x2[R][C] = M[row][col];
					C++;
				}
			R++;
		}
}

void transpose(float M[3][3])
{
	for (int r=0 ; r<3 ; r++)
		for (int c=r+1 ; c<3 ; c++)
		{
			float t = M[r][c];
			M[r][c] = M[c][r];
			M[c][r] = t;
		}
}

void transpose(float M[3][3], float MT[3][3])
{
	for (int r=0 ; r<3 ; r++)
		for (int c=0 ; c<3 ; c++)
			MT[r][c] = M[r][c];
	transpose(MT);
}

void upper3x3(float M4x4[4][4], float U3x3[3][3])
{
	for (int r=0 ; r<3 ; r++)
		for (int c=0 ; c<3 ; c++)
			U3x3[r][c] = M4x4[r][c];
}
