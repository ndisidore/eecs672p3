// AffVector.cxx -- 3D vectors associated with a 3D affine space

/*
Copyright (c) 1998-2012, cryph Component Library and Tools
James R. Miller, University of Kansas, Lawrence, Kansas, U.S.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

   * Neither the name of the cryph Component Library and Tools
     project nor the names of its contributors may be used to endorse
     or promote products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS OPEN SOURCE. IT IS NOT IN THE PUBLIC DOMAIN.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>

#include "AffPoint.h"
#include "AffVector.h"
#include "Inline.h"

using namespace std;

namespace cryph
{

// ---------- Global constants

// 1. Special Vectors
const AffVector	AffVector::xu = AffVector( 1.0 , 0.0 , 0.0 );
const AffVector	AffVector::yu = AffVector( 0.0 , 1.0 , 0.0 );
const AffVector	AffVector::zu = AffVector( 0.0 , 0.0 , 1.0 );
const AffVector	AffVector::zeroVector = AffVector( 0.0 , 0.0 , 0.0 );

// 2. Values for "ioAspect" in "ioFormat"
const int AffVector::openDelimiter = 1;
const int AffVector::separator     = 2;

// class variables
IOSpec AffVector::iSpec(' ',' ');
IOSpec AffVector::oSpec('(',',');

// ---------- General Class Methods

void
AffVector::coordinateSystemFromUW(AffVector& U, AffVector& V, AffVector& W)
{
	// We assume that U and W have values (even if all zero). We use W (after
	// normalization) as the W axis. The component of U perpendicular to W
	// (and after normalization) becomes U. Finally V is the cross product
	// of W and U. If troubles arise, suitable defaults are generated so that
	// this routine always does something reasonable.

	if (W.normalize() < BasicDistanceTol)
	{
		U = AffVector::xu; V = AffVector::yu; W = AffVector::zu;
	}
	else
	{
		AffVector par, perp;

		W.decompose(U,par,perp);
		if (perp.normalizeToCopy(U) < BasicDistanceTol)
		{
			W.arbitraryNormal(U);
			(void) U.normalize();
		}
		V = AffVector::cross(W,U);
		V.normalize(); // for numerical reasons
	}
}

void
AffVector::coordinateSystemFromVW(AffVector& U, AffVector& V, AffVector& W)
{
	// We assume that V and W have values (even if all zero). We use W (after
	// normalization) as the W axis. The component of V perpendicular to W
	// (and after normalization) becomes V. Finally U is the cross product
	// of V and W. If troubles arise, suitbale defaults are generated so that
	// this routine always does something reasonable.

	if (W.normalize() < BasicDistanceTol)
	{
		U = AffVector::xu; V = AffVector::yu; W = AffVector::zu;
	}
	else
	{
		AffVector par, perp;

		W.decompose(V,par,perp);
		if (perp.normalizeToCopy(V) < BasicDistanceTol)
		{
			W.arbitraryNormal(V);
			(void) V.normalize();
		}
		U = AffVector::cross(V,W);
		U.normalize(); // for numerical reasons
	}
}

AffVector
AffVector::cross(const AffVector& v1, const AffVector& v2)
{
	AffVector	result;

	result.dx = v1.dy*v2.dz - v1.dz*v2.dy;

	result.dy = v1.dz*v2.dx - v1.dx*v2.dz;

	result.dz = v1.dx*v2.dy - v1.dy*v2.dx;

	return result;
}

char*
AffVector::getInputFormat(int iAspect)
{
	switch (iAspect)
	{
		case AffVector::openDelimiter:
			return copyString(AffVector::iSpec.getOpenDelim());
		case AffVector::separator:
			return copyString(AffVector::iSpec.getSeparator());
	}
	return NULL;
}

char*
AffVector::getOutputFormat(int oAspect)
{
	switch (oAspect)
	{
		case AffVector::openDelimiter:
			return copyString(AffVector::oSpec.getOpenDelim());
		case AffVector::separator:
			return copyString(AffVector::oSpec.getSeparator());
	}
	return NULL;
}

void
AffVector::inputFormat(int iAspect, char iValue)
{
	char    val[] = { iValue, '\0' };
	inputFormat(iAspect,val);
}

void
AffVector::inputFormat(int iAspect, const char* iValue)
{
	switch (iAspect)
	{
		case AffVector::openDelimiter:
			AffVector::iSpec.setOpenDelim(iValue);
			break;
		case AffVector::separator:
			AffVector::iSpec.setSeparator(iValue);
			break;
		default:
			; // ignore for now
	}
}
void
AffVector::ioFormat(int ioAspect, char ioValue)
{
	char    val[] = { ioValue, '\0' };
	ioFormat(ioAspect,val);
}

void
AffVector::ioFormat(int ioAspect, const char* ioValue)
{
	inputFormat(ioAspect,ioValue);
	outputFormat(ioAspect,ioValue);
}

void
AffVector::outputFormat(int oAspect, char oValue)
{
	char    val[] = { oValue, '\0' };
	outputFormat(oAspect,val);
}

void
AffVector::outputFormat(int oAspect, const char* oValue)
{
	switch (oAspect)
	{
		case AffVector::openDelimiter:
			AffVector::oSpec.setOpenDelim(oValue);
			break;
		case AffVector::separator:
			AffVector::oSpec.setSeparator(oValue);
			break;
		default:
			; // ignore for now
	}
}

// --------- basic constructors

AffVector::AffVector() : dx(0), dy(0), dz(0)
{
}

AffVector::AffVector(double Dx, double Dy, double Dz) : dx(Dx), dy(Dy), dz(Dz)
{
}
AffVector::AffVector(const AffVector& v) : dx(v.dx), dy(v.dy), dz(v.dz)
{
}

// --------- other constructors

AffVector::AffVector(const AffPoint& p) : dx(p[X]), dy(p[Y]), dz(p[Z])
{
}
AffVector::AffVector(const double xyz[3]) : dx(xyz[0]), dy(xyz[1]), dz(xyz[2])
{
}
AffVector::AffVector(const float xyz[3]) : dx(xyz[0]), dy(xyz[1]), dz(xyz[2])
{
}

// --------- destructor

AffVector::~AffVector()
{
}

// --------- overloaded operators

AffVector
AffVector::operator=(const AffVector& rhs)
{
	dx = rhs.dx; dy = rhs.dy; dz = rhs.dz;
	return *this;
}

AffVector
AffVector::operator+=(const AffVector& rhs)
{
	dx += rhs.dx; dy += rhs.dy; dz += rhs.dz;
	return *this;
}

AffVector
AffVector::operator-=(const AffVector& rhs)
{
	dx -= rhs.dx; dy -= rhs.dy; dz -= rhs.dz;
	return *this;
}

AffVector
AffVector::operator*=(double f)
{
	dx *= f;
	dy *= f;
	dz *= f;

	return *this;
}

AffVector
AffVector::operator/=(double f)
{
	dx /= f;
	dy /= f;
	dz /= f;

	return *this;
}

bool
AffVector::operator==(const AffVector& rhs)
{
	AffVector	u, v;

	double	thisL = this->normalizeToCopy(u);
	double	rhsL  = rhs.normalizeToCopy(v);

	if (fabs(thisL-rhsL) < BasicDistanceTol)
		if (thisL > BasicDistanceTol)
		{
			if (fabs(AffVector::dot(u,v)-1.0) < BasicUnitTol)
				return true;
		}
		else // both are zero vectors, hence:
			return true;

	return false;
}

bool
AffVector::operator!=(const AffVector& rhs)
{
	return ! this->operator==(rhs);
}

double
AffVector::operator[](int index) const
{
	// read-only indexing

	switch (index)
	{
		case DW:
			return 0.0; // w == 0
		case DX:
			return dx;
		case DY:
			return dy;
		case DZ:
			return dz;
	}

	return 0.0;
}

// operators as external functions, not methods

ostream&
operator<<(ostream& os, const AffVector& v)
{
	IOSpec* outSpec = AffVector::getOutputSpec();
	os << outSpec->getOpenDelim();
	outputElement(os, v[DX]);
	os << outSpec->getSeparator();
	outputElement(os, v[DY]);
	os << outSpec->getSeparator();
	outputElement(os, v[DZ]);
	os << outSpec->getCloseDelim();
	return os;
}
istream&
operator>>(istream& is, AffVector& v)
{
	IOSpec* inpSpec = AffVector::getInputSpec();
	double x, y, z;
	skipNonblankChars(is,inpSpec->numNonblankDelimChars());

	is >> x;

	skipNonblankChars(is,inpSpec->numNonblankSeparatorChars());

	is >> y;

	skipNonblankChars(is,inpSpec->numNonblankSeparatorChars());

	is >> z;

	skipNonblankChars(is,inpSpec->numNonblankDelimChars());

	v = AffVector(x,y,z);

	return is;
}

// ---------- General Instance Methods

void
AffVector::arbitraryNormal(AffVector& normal) const
{
	// Create an arbitrary vector perpendicular to "this"

	const int ignoreX = 0;
	const int ignoreY = 1;
	const int ignoreZ = 2;

	double	Dx = fabs(this->dx);
	double	Dy = fabs(this->dy);
	double	Dz = fabs(this->dz);

	int		ignore = ignoreX;
	if (Dx >= Dy)
		if (Dy > Dz)
			ignore = ignoreZ;
		else
			ignore = ignoreY;
	else if (Dx > Dz)
		ignore = ignoreZ;

	// set the ignored component to zero, invert the other two, and negate the
	// one with the largest absolute value.

	switch (ignore)
	{
		case ignoreX:
			normal.dx = 0.0;
			if (Dy > Dz)
			{
				normal.dy = this->dz; normal.dz = -this->dy;
			}
			else
			{
				normal.dy = -this->dz; normal.dz = this->dy;
			}
			break;
		case ignoreY:
			normal.dy = 0.0;

			// to get "preferred result" when v1 parallel to z-axis, we add the
			// "or dx < BasicDistanceTol" business here

			if ( (Dx > Dz) || (Dx < BasicDistanceTol) )
			{
				normal.dx = this->dz; normal.dz = -this->dx;
			}
			else
			{
				normal.dx = -this->dz; normal.dz = this->dx;
			}
			break;
		case ignoreZ:
			normal.dz = 0.0;
			if (Dx > Dy)
			{
				normal.dx = this->dy; normal.dy = -this->dx;
			}
			else
			{
				normal.dx = -this->dy; normal.dy = this->dx;
			}
	}
}
	
void AffVector::vComponents(double* components, int offset) const
{
	components[offset  ] = dx;
	components[offset+1] = dy;
	components[offset+2] = dz;
}

void AffVector::vComponents(float* components, int offset) const
{
	components[offset  ] = static_cast<float>(dx);
	components[offset+1] = static_cast<float>(dy);
	components[offset+2] = static_cast<float>(dz);
}

AffVector
AffVector::cross(const AffVector& rhs) const
{
	AffVector	result;

	result.dx = dy*rhs.dz - dz*rhs.dy;

	result.dy = dz*rhs.dx - dx*rhs.dz;

	result.dz = dx*rhs.dy - dy*rhs.dx;

	return result;
}

void
AffVector::decompose(const AffVector& arbitraryVector,
			AffVector& parallel, AffVector& perpendicular) const
{
	AffVector	u;
	this->normalizeToCopy(u);

	parallel = AffVector::dot(arbitraryVector,u) * u;
	perpendicular = arbitraryVector - parallel;
}

double
AffVector::maxAbsComponent(int& componentIndex) const
{
	componentIndex = DX;
	double v = fabs(dx);
	if (fabs(dy) > v)
	{
		v = fabs(dy); componentIndex = DY;
	}
	if (fabs(dz) > v)
	{
		v = fabs(dz); componentIndex = DZ;
	}
	return v;
}

double
AffVector::minAbsComponent(int& componentIndex) const
{
	componentIndex = DX;
	double v = fabs(dx);
	if (fabs(dy) < v)
	{
		v = fabs(dy); componentIndex = DY;
	}
	if (fabs(dz) < v)
	{
		v = fabs(dz); componentIndex = DZ;
	}
	return v;
}

double
AffVector::normalize()
{
	double l = length();
	if (l > 0.0)
	{
		dx /= l; dy /= l; dz /= l;
	}
	return l;
}

double
AffVector::normalizeToCopy(AffVector& normalizedCopy) const
{
	normalizedCopy = *this;
	return normalizedCopy.normalize();
}

bool
AffVector::parallelTo(const AffVector& v) const
{
	AffVector	v1, v2;
	this->normalizeToCopy(v1);
	v.normalizeToCopy(v2);
	double diff = fabs( 1.0 - fabs(AffVector::dot(v1,v2)));
	return (diff < BasicUnitTol);
}

// caller responsible to do delete[] in "toCompArray"
double* AffVector::toCompArray(AffVector* vecs, int nVecs)
{
	if (nVecs <= 0)
		return NULL;
	double* buffer = new double[3*nVecs];
	int loc = 0;
	for (int i=0 ; i<nVecs ; i++, loc+=3)
	{
		buffer[loc  ] = vecs[i].dx;
		buffer[loc+1] = vecs[i].dy;
		buffer[loc+2] = vecs[i].dz;
	}
	return buffer;
}
	
// in following, a private (to AffVector) array is used and the caller
// cannot rely on the data staying untouched for long. (Next call to
// this method will overwrite and possible relocate the array.) This
// routine is useful for feeding OpenGL routines like glNormalPointer.
double* AffVector::transientBuffer = NULL;
int AffVector::transientBufferLength = 0;
	
double* AffVector::toTransientCompArray(AffVector* vecs, int nVecs)
{
	if (nVecs <= 0)
		return NULL;
	if (nVecs > transientBufferLength)
	{
		if (transientBuffer != NULL)
			delete[] transientBuffer;
		transientBuffer = new double[3*nVecs];
		transientBufferLength = nVecs;
	}
	int loc = 0;
	for (int i=0 ; i<nVecs ; i++, loc+=3)
	{
		transientBuffer[loc  ] = vecs[i].dx;
		transientBuffer[loc+1] = vecs[i].dy;
		transientBuffer[loc+2] = vecs[i].dz;
	}
	return transientBuffer;
}
}
