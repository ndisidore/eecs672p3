#include <iostream>

#include "AffPoint.h"
#include "AffVector.h"
#include "MatrixUtil.h"

int makeCircle(const cryph::AffPoint& C, const cryph::AffVector& w, double radius, float*& gpuBuffer);

void demoMatrix()
{
	float M4x4[4][4] =
	{
		{-2, 1, 3, 2},
		{1, 2, 2, 0},
		{-1, -1, 2, -1},
		{4, 3, 1, 0}
	};
	float M3x3[3][3], M3x3Inv[3][3], M3x3InvTranspose[3][3];
	upper3x3(M4x4, M3x3);
	if (inverse(M3x3, M3x3Inv))
	{
		transpose(M3x3Inv, M3x3InvTranspose);
		float shouldBeIdentity[3][3];
		multiply(M3x3, M3x3Inv, shouldBeIdentity);
		std::cout << "M3x3:\n";
		showMatrix(std::cout, M3x3);
		std::cout << "M3x3Inv:\n";
		showMatrix(std::cout, M3x3Inv);
		std::cout << "M3x3InvTranspose\n";
		showMatrix(std::cout, M3x3InvTranspose);
		std::cout << "shouldBeIdentity:\n";
		showMatrix(std::cout, shouldBeIdentity);
	}
	else
		std::cerr << "Could not invert the matrix!\n";
}

void demoPointVector()
{
	cryph::AffPoint P(-1, 1, 0), Q(1, 1, 0), R(-3, 1, 2);
	std::cout << "P = " << P << "; Q = " << Q << "; R = " << R << std::endl;
	cryph::AffVector u = R - Q;
	cryph::AffVector v = P - Q;
	cryph::AffVector n = u.cross(v);
	std::cout << "Compute the normal to triangle PQR:\n";
	std::cout << "\tu = R-Q = " << u << "; v = P-Q " << v << "; n = u x v = " << n << std::endl;

	// vector decomposition
	u.normalize();  // "u" is now a "uHat"...
	// decompose an arbitrary vector, v, into its components parallel and perpendicular to uHat:
	cryph::AffVector vPar, vPerp;
	u.decompose(v, vPar, vPerp);
	std::cout << "uHat = " << u << "; v = " << v << "; vPar = " << vPar << "; vPerp = " << vPerp << std::endl;
	// There are several ways to demonstrate we got the right results
	cryph::AffVector sum = vPar + vPerp;
	std::cout << "Sum of vPar and vPer (should be original V): " << sum << '\n';
	double testDot = u.dot(vPerp); // should be zero!
	std::cout << "testDot should be zero. It is: " << testDot << std::endl;
	cryph::AffVector testVec = u.cross(vPar); // should be a zero vector!
	std::cout << "testVec should be a zero vector. It is: " << testVec << std::endl;

	// point coordinates can be pulled out into a simple array of float or double:
	double xyzD[3];
	P.aCoords(xyzD);
	std::cout << "P's extracted double coordinates:";
	for (int i=0 ; i<3 ; i++)
		std::cout << ' ' << xyzD[i];
	std::cout << std::endl;
	float xyzF[3];
	P.aCoords(xyzF);
	std::cout << "P's extracted float coordinates: ";
	for (int i=0 ; i<3 ; i++)
		std::cout << ' ' << xyzF[i];
	std::cout << std::endl;

	// Suppose we want to create a circle centered at C in the plane determined by (C,w)
	cryph::AffPoint C(-3.2, 1.7, 4.1); // some arbitrary point we will want to use as the center of a circle
	cryph::AffVector w(-1, 3, 2.7); // some arbitrary normal for the plane of the circle
	float* gpuBuffer = NULL; // allocate space for and store the points here
	int nPoints = makeCircle(C,w,7.3,gpuBuffer);
	// here we send the data to the GPU
	std::cout << "I'm busy sending " << nPoints << " points to the GPU!\n";
	// glBufferData(...);
	// OK, now we can get rid of the buffer:
	delete [] gpuBuffer;

	// You can do all this with vector components, too. That may be useful for populating
	// an array of normal vectors for the lighting model. For example:
	float components[3*nPoints];
	int offset = 0;
	for (int i=0 ; i<nPoints ; i++)
	{
		// compute the next desired value for vector, v
		// ...

		// store this new value in the array and advance the offset
		v.vComponents(components, offset);
		offset += 3;
	}
}

int makeCircle(const cryph::AffPoint& C, const cryph::AffVector& wIn, double radius, float*& gpuBuffer)
{
	// first, we need to create a local coordinate system for the points:
	cryph::AffVector u, v, w(wIn); // 'u' and 'v' are initialized to (0, 0, 0)
	cryph::AffVector::coordinateSystemFromUW(u,v,w);
	// (u, v, w) now are mutually perpendicular unit vectors

	// Now we can create points around the circle:

	// An optional "offset" parameter can be used to extract a series of points into an
	// array one after the other. Here we are creating a piecewise linear approximation
	// to a circle in the (C, w) plane, centered on C. Note how the various algebraic
	// operators are overloaded to work on AffPoint and AffVector instances.
	const int NUM_POINTS = 200;
	cryph::AffPoint buf[NUM_POINTS];

	// client side computations will be done in double (since that is how AffPoint, et al.
	// are implemented), but we need to create single precision float to go to GPU, hence
	// the gpuBuffer must be float:
	gpuBuffer = new float[3*NUM_POINTS]; // '3': x, y, z
	// We will use a triangle fan to crteate a filled-in circle. Hence the first point is:
	buf[0] = C;
	buf[0].aCoords(gpuBuffer); // stores first three coordinates
	int offset = 3; // where the next point will go
	double theta = 0.0;
	// Note "-2" in next line: "-1" for center; another "-1" so that last circle
	// point is same as first
	double dTheta = 2.0*M_PI/(NUM_POINTS-2);
	for (int i=1 ; i<NUM_POINTS ; i++)
	{
		buf[i] = C + radius * (cos(theta)*u + sin(theta)*v);
		buf[i].aCoords(gpuBuffer, offset);
		offset += 3; // where the next point will go
	}
	// Now "gpuBuffer" is full of data that can be passed to the GPU via a glBufferData call
	return NUM_POINTS;
}

int main()
{
	demoPointVector();
	demoMatrix();

	return 0;
}
