// IOSpec.c++ -- implementation of class which manages io specifications for
//               points, vectors, matrices and the like.

/*
Copyright (c) 1998-2012, cryph Component Library and Tools
James R. Miller, University of Kansas, Lawrence, Kansas, U.S.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

   * Neither the name of the cryph Component Library and Tools
     project nor the names of its contributors may be used to endorse
     or promote products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS OPEN SOURCE. IT IS NOT IN THE PUBLIC DOMAIN.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include <ctype.h>
#include <string.h>

#include "IOSpec.h"

namespace cryph
{

IOSpec::IOSpec()
{
	setOpenDelim('(');
	setSeparator(',');
}

IOSpec::IOSpec(char delim, char sep)
{
	setOpenDelim(delim);
	setSeparator(sep);
}

IOSpec::IOSpec(char* delim, char* sep)
{
	setOpenDelim(delim);
	setSeparator(sep);
}

IOSpec::~IOSpec()
{
}

char IOSpec::inverse(char c)
{
	switch(c)
	{
		case '(':
			return ')';
		case '[':
			return ']';
		case '{':
			return '}';
		case '<':
			return '>';
		default:
			return c;
	}
}

void IOSpec::setOpenDelim(char delim)
{
	char	newDelim[] = { delim, '\0' };
	setOpenDelim(newDelim);
}

void IOSpec::setOpenDelim(const char* delim)
{
	int last = strlen(delim);
	if (last > (MaxDelimLength-1))
		last = MaxDelimLength - 1;

	mOpenDelim[last] = mCloseDelim[last] = '\0';

	mNumNonblankDelimChars = 0;
	int i_open = 0;
	int i_close = last-1;
	while (i_open < last)
	{
		if (!isspace(delim[i_open]))
			mNumNonblankDelimChars++;

		mOpenDelim[i_open] = delim[i_open];
		mCloseDelim[i_close] = inverse(delim[i_open]);
		i_open++;
		i_close--;
	}
}

void IOSpec::setSeparator(char separator)
{
	char	newSep[] = { separator, '\0' };
	setSeparator(newSep);
}

void IOSpec::setSeparator(const char* separator)
{
	int last = strlen(separator);
	if (last > (MaxSeparatorLength-1))
		last = MaxSeparatorLength - 1;

	mNumNonblankSeparatorChars = 0;
	for (int i=0 ; i<last ; i++)
	{
		mSeparator[i] = separator[i];
		if (!isspace(mSeparator[i]))
			mNumNonblankSeparatorChars++;
	}
	mSeparator[last] = '\0';
}

}
