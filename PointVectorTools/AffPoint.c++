// AffPoint.c++ -- 3D Affine points

/*
Copyright (c) 1998-2012, cryph Component Library and Tools
James R. Miller, University of Kansas, Lawrence, Kansas, U.S.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

   * Neither the name of the cryph Component Library and Tools
     project nor the names of its contributors may be used to endorse
     or promote products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS OPEN SOURCE. IT IS NOT IN THE PUBLIC DOMAIN.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>

#include "AffPoint.h"
#include "AffVector.h"
#include "Inline.h"

namespace cryph
{

// ---------- Global constants

// 1. Special Points
const AffPoint AffPoint::origin     = AffPoint(0.0 , 0.0 , 0.0);
const AffPoint AffPoint::xAxisPoint = AffPoint(1.0 , 0.0 , 0.0);
const AffPoint AffPoint::yAxisPoint = AffPoint(0.0 , 1.0 , 0.0);
const AffPoint AffPoint::zAxisPoint = AffPoint(0.0 , 0.0 , 1.0);

// 2. values for "ioAspect" in "ioFormat"
const int AffPoint::openDelimiter  = 1;
const int AffPoint::separator      = 2;
const int AffPoint::closeDelimiter = 3;

// Class variables

double	AffPoint::sCoincidenceTol = BasicDistanceTol;
IOSpec	AffPoint::iSpec(' ',' ');
IOSpec	AffPoint::oSpec('(',',');

// --------- basic constructors
	
AffPoint::AffPoint() : x(0), y(0), z(0)
{
}

AffPoint::AffPoint(double xx, double yy, double zz) : x(xx), y(yy), z(zz)
{
}

AffPoint::AffPoint(const AffPoint& p) : x(p.x), y(p.y), z(p.z)
{
}

// --------- other constructors

AffPoint::AffPoint(const double* p) : x(p[0]), y(p[1]), z(p[2])
{
}

AffPoint::AffPoint(const float* p) : x(p[0]), y(p[1]), z(p[2])
{
}

AffPoint::AffPoint(const AffVector& v) : x(v[DX]), y(v[DY]), z(v[DZ])
{
}

// --------- destructor

AffPoint::~AffPoint()
{
}

// --------- General Class Methods

AffPoint
AffPoint::centroid(const AffPoint p[], int nPoints)
{
	AffPoint	sum(p[0]);
	for (int i=1 ; i<nPoints ; i++)
		sum = sum + p[i];
	return (sum / (double)nPoints);
}

char*
AffPoint::getInputFormat(int iAspect)
{
	switch (iAspect)
	{
		case AffPoint::openDelimiter:
			return copyString(AffPoint::iSpec.getOpenDelim());
		case AffPoint::separator:
			return copyString(AffPoint::iSpec.getSeparator());
	}
	return NULL;
}

char*
AffPoint::getOutputFormat(int oAspect)
{
	switch (oAspect)
	{
		case AffPoint::openDelimiter:
			return copyString(AffPoint::oSpec.getOpenDelim());
		case AffPoint::separator:
			return copyString(AffPoint::oSpec.getSeparator());
		case AffPoint::closeDelimiter:
			return copyString(AffPoint::oSpec.getCloseDelim());
	}
	return NULL;
}

void
AffPoint::inputFormat(int iAspect, char iValue)
{
	char	val[] = { iValue, '\0' };
	inputFormat(iAspect,val);
}

void
AffPoint::inputFormat(int iAspect, const char* iValue)
{
	switch (iAspect)
	{
		case AffPoint::openDelimiter:
			AffPoint::iSpec.setOpenDelim(iValue);
			break;
		case AffPoint::separator:
			AffPoint::iSpec.setSeparator(iValue);
			break;
		default:
			; // ignore for now
	}
}

void
AffPoint::ioFormat(int ioAspect, char ioValue)
{
	char	val[] = { ioValue, '\0' };
	ioFormat(ioAspect,val);
}

void
AffPoint::ioFormat(int ioAspect, const char* ioValue)
{
	AffPoint::inputFormat(ioAspect,ioValue);
	AffPoint::outputFormat(ioAspect,ioValue);
}

double
AffPoint::maxOffsetInDirection(const AffPoint& ref, const AffVector& dir,
			const AffPoint buf[], int bufSize, int& index1, int& index2)
{
	index1 = index2 = -1;
	if (bufSize < 1)
		return 0.0;

	index1 = index2 = 0;
	double offset = dir.dot(buf[0] - ref);
	for (int i=1 ; i<bufSize ; i++)
	{
		double t = dir.dot(buf[i] - ref);
		if (equalScalars(t,offset,BasicDistanceTol))
		{
			if (i == index2+1)
				index2++;
		}
		else if (t > offset)
		{
			index1 = index2 = i;
			offset = t;
		}
	}
	return offset;
}

void
AffPoint::outputFormat(int oAspect, char oValue)
{
	char	val[] = { oValue, '\0' };
	outputFormat(oAspect,val);
}

void
AffPoint::outputFormat(int oAspect, const char* oValue)
{
	switch (oAspect)
	{
		case AffPoint::openDelimiter:
			AffPoint::oSpec.setOpenDelim(oValue);
			break;
		case AffPoint::separator:
			AffPoint::oSpec.setSeparator(oValue);
			break;
		default:
			; // ignore for now
	}
}

double
AffPoint::ratio(const AffPoint& a, const AffPoint& b, const AffPoint& c)
{
	AffVector u = c - a;
	if (u.normalize() > 0.0)
	{
		double denom = AffVector::dot((c-b),u);
		if (fabs(denom) > BasicDistanceTol)
			return AffVector::dot((b-a),u) / denom;
	}
	return 0.0;
}

void
AffPoint::setCoincidenceTolerance(double tol)
{
	if (tol >= 0.0) // really better be strictly > 0!!!
		AffPoint::sCoincidenceTol = tol;
}

// --------- operators as methods

AffPoint
AffPoint::operator=(const AffPoint& rhs)
{
	x = rhs.x; y = rhs.y ; z = rhs.z;
	return *this;
}

AffPoint
AffPoint::operator+=(const AffVector& rhs)
{
	x += rhs[DX]; y += rhs[DY] ; z += rhs[DZ];
	return *this;
}

AffPoint
AffPoint::operator+=(const AffPoint& rhs)
{
	x += rhs.x; y += rhs.y ; z += rhs.z;
	return *this;
}

AffPoint
AffPoint::operator-=(const AffVector& rhs)
{
	x -= rhs[DX]; y -= rhs[DY] ; z -= rhs[DZ];
	return *this;
}

AffPoint
AffPoint::operator*=(double f)
{
	x *= f; y *= f ; z *= f;
	return *this;
}

AffPoint
AffPoint::operator/=(double f)
{
	x /= f; y /= f ; z /= f;
	return *this;
}
double
AffPoint::operator[](int index) const
{
	// read-only indexing

	switch (index)
	{
		case W:
			return 1.0; // w == 1
		case X:
			return x;
		case Y:
			return y;
		case Z:
			return z;
	}

	return 0.0;
}

// operators that are external functions, not methods

std::ostream&
operator<<(std::ostream& os, const AffPoint& p)
{
	IOSpec* outSpec = AffPoint::getOutputSpec();
	os << outSpec->getOpenDelim();
	outputElement(os, p[X]);
	os << outSpec->getSeparator();
	outputElement(os, p[Y]);
	os << outSpec->getSeparator();
	outputElement(os, p[Z]);
	os << outSpec->getCloseDelim();
	return os;
}
std::istream&
operator>>(std::istream& is, AffPoint& p)
{
	IOSpec* inpSpec = AffPoint::getInputSpec();
	double x, y, z;
	skipNonblankChars(is,inpSpec->numNonblankDelimChars());

	is >> x;

	skipNonblankChars(is,inpSpec->numNonblankSeparatorChars());

	is >> y;

	skipNonblankChars(is,inpSpec->numNonblankSeparatorChars());

	is >> z;

	skipNonblankChars(is,inpSpec->numNonblankDelimChars());

	p = AffPoint(x,y,z);

	return is;
}

// --------- General Instance Methods

double*
AffPoint::aCoords(double* coords, int offset) const
{
	// extract affine coords and place into a double precision array

	coords[offset] = x;
	coords[offset+1] = y;
	coords[offset+2] = z;
	return coords;
}

float*
AffPoint::aCoords(float* coords, int offset) const
{
	// extract affine coords and place into a single precision (float) array

	coords[offset] = static_cast<float>(x);
	coords[offset+1] = static_cast<float>(y);
	coords[offset+2] = static_cast<float>(z);
	return coords;
}

#define TESTING 0

void
AffPoint::barycentricCoords( // in a plane
	// find the areal Barycentric coordinates of "this" point with respect to:
	const AffPoint& P1, const AffPoint& P2, const AffPoint& P3,
	// returning them in:  (b1+b2+b3 = 1)
	double& b1, double& b2, double& b3) const
{
	AffVector nToTriangle = (P2-P1).cross(P3-P1);
	// b1:
	AffVector ref = nToTriangle.cross(P3-P2);
	ref.normalize();
	double triangleHeight  = (P1 - P2).dot(ref);
	double signedHeightFromPoint = (*this - P2).dot(ref);
	b1 = signedHeightFromPoint/triangleHeight;

	// b2:
	ref = nToTriangle.cross(P1-P3);
	ref.normalize();
	triangleHeight  = (P2 - P3).dot(ref);
	signedHeightFromPoint = (*this - P3).dot(ref);
	b2 = signedHeightFromPoint/triangleHeight;

	// b3:
#if TESTING
	ref = nToTriangle.cross(P2 - P1);
	ref.normalize();
	triangleHeight  = (P3 - P1).dot(ref);
	signedHeightFromPoint = (*this - P1).dot(ref);
	b3 = signedHeightFromPoint/triangleHeight;
#else
	b3 = 1.0 - b1 - b2;
#endif
}

void
AffPoint::barycentricCoords( // on a line
	// find the areal Barycentric coordinates of "this" point with respect to:
	const AffPoint& P1, const AffPoint& P2,
	// returning them in:  (b1+b2 = 1)
	double& b1, double& b2) const
{
	AffVector u = P2 - P1;
	double dist = u.normalize();
	if (fabs(dist) < BasicDistanceTol)
	{
		b1 = 1.0; b2 = 0.0;
		return;
	}
	b2 = u.dot(*this - P1) / dist;
	b1 = 1.0 - b2;
}

bool
AffPoint::coincidentWith(const AffPoint& p) const
{
	return (this->distanceTo(p) < AffPoint::sCoincidenceTol);
}

double
AffPoint::distanceFromLine(const AffPoint& B, const AffVector& u) const
{
	return sqrt( distanceSquaredFromLine(B,u) );
}

double
AffPoint::distanceFromOrigin() const
{
	return sqrt( distanceSquaredFromOrigin() );
}

double
AffPoint::distanceSquaredFromLine(const AffPoint& B, const AffVector& u) const
{
	// Pythagorean theorem approach: c^2 = a^2 + b^2. The distance squared
	// we seek is b^2:
	AffVector toPoint = *this - B;
	double cSquared = toPoint.dot(toPoint);
	AffVector dir;
	u.normalizeToCopy(dir);
	double aSquared = dir.dot(toPoint);
	aSquared *= aSquared;
	double distSquared = cSquared - aSquared;
	if (distSquared < 0.0)
		// must be a tiny number that really ought to be 0.0
		distSquared = 0.0;
	return distSquared;
}

double
AffPoint::distanceSquaredFromOrigin() const
{
	return x*x + y*y + z*z;
}

double
AffPoint::distanceSquaredTo(const AffPoint& p) const
{
	double t = (x - p.x);
	double s = t*t;
	t = (y - p.y);
	s += t*t;
	t = (z - p.z);
	s += t*t;

	return s;
}

double
AffPoint::distanceTo(const AffPoint& p) const
{
	return sqrt( distanceSquaredTo(p) );
}

double
AffPoint::normalize()
{
	double f = distanceFromOrigin();
	if (f < AffPoint::sCoincidenceTol)
		return 0.0;

	x /= f; y /= f; z /= f;
	return f;
}

double*
AffPoint::pCoords(double* coords, double w) const
{
	return pCoords(coords, w, 0);
}

double*
AffPoint::pCoords(double* coords, double w, int offset) const
{
	// extract coords and represent in projective space using the provided "w" 
	// place into a double precision array

	coords[offset] = w*x;
	coords[offset+1] = w*y;
	coords[offset+2] = w*z;
	coords[offset+3] = w;
	return coords;
}

float*
AffPoint::pCoords(float* coords, double w) const
{
	return pCoords(coords, w, 0);
}

float*
AffPoint::pCoords(float* coords, double w, int offset) const
{
	// extract coords and represent in projective space using the provided "w"
	// place into a single precision array

	coords[offset] = static_cast<float>(w*x);
	coords[offset+1] = static_cast<float>(w*y);
	coords[offset+2] = static_cast<float>(w*z);
	coords[offset+3] = static_cast<float>(w);
	return coords;
}

// caller responsible to do delete[] in "toCoordArray"
double* AffPoint::toCoordArray(AffPoint* pts, int nPts)
{
	if (nPts <= 0)
		return NULL;
	double* buffer = new double[3*nPts];
	int loc = 0;
	for (int i=0 ; i<nPts ; i++, loc+=3)
		pts[i].aCoords(&buffer[loc]);
	return buffer;
}
	
// in following, a private (to AffPoint) array is used and the caller
// cannot rely on the data staying untouched for long. (Next call to
// this method will overwrite and possible relocate the array.) This
// routine is useful for feeding OpenGL routines like glVertexPointer.
	
double* AffPoint::transientBuffer = NULL;
int AffPoint::transientBufferLength = 0;
	
double* AffPoint::toTransientCoordArray(AffPoint* pts, int nPts)
{
	if (nPts <= 0)
		return NULL;
	if (nPts > transientBufferLength)
	{
		if (transientBuffer != NULL)
			delete[] transientBuffer;
		transientBuffer = new double[3*nPts];
		transientBufferLength = nPts;
	}
	int loc = 0;
	for (int i=0 ; i<nPts ; i++, loc+=3)
		pts[i].aCoords(&transientBuffer[loc]);
	return transientBuffer;
}
	
void
AffPoint::toCylindrical(double& r, double& theta, double& z) const
{
	z = this->z;
	theta = atan2(this->y, this->x);
	r = sqrt( (this->x)*(this->x) + (this->y)*(this->y) );
}

void
AffPoint::toSpherical(double& rho, double& theta, double& phi) const
{
	AffVector	v(*this); // "v" is the vector from the origin to "this" point
	rho = v.normalize();
	if (rho < BasicDistanceTol)
		// the point is at the origin, hence:
		rho = theta = phi = 0.0;
	else
	{
		AffVector	vPerp(v[DX], v[DY], 0.0);
		double r = vPerp.normalize();
		if (r < BasicDistanceTol)
		{
			// point is on the z-axis ==> phi=0 or PI; theta is arbitrary:
			theta = 0.0;
			if (this->z > 0.0)
				phi = 0.0;
			else
				phi = PI;
		}
		else
		{
			theta = atan2(vPerp[DY],vPerp[DX]);
			phi   = atan2(r/rho, v[DZ]/rho);
		}
	}
}

}
